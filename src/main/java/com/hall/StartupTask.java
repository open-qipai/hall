package com.hall;

import com.hall.service.MoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class StartupTask {

    private final ExecutorService executorService = Executors.newFixedThreadPool(2);
    @Autowired
    private MoneyService moneyService;

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationReady() {
        executorService.submit(this::sendAllUserInitGold);
    }

    private void sendAllUserInitGold() {
        moneyService.sendAllUserInitGold(20000, System.currentTimeMillis());
    }

}