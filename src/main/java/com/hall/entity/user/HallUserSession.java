package com.hall.entity.user;

import dml.user.entity.UserSession;

public class HallUserSession implements UserSession {
    private String id;
    private long userId;

    public HallUserSession() {
    }

    public HallUserSession(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Object getUserID() {
        return userId;
    }

    @Override
    public void setUserID(Object userID) {
        this.userId = (long) userID;
    }
}
