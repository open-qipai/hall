package com.hall.entity.allocation;

import dml.gameserverallocation.loadbased.entity.LoadBalancedAllocationGameServerBase;

public class AllocationGameServer extends LoadBalancedAllocationGameServerBase {
    private String id;
    private String gameId;
    private String innerHost;
    private String outerHost;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getInnerHost() {
        return innerHost;
    }

    public void setInnerHost(String innerHost) {
        this.innerHost = innerHost;
    }

    public String getOuterHost() {
        return outerHost;
    }

    public void setOuterHost(String outerHost) {
        this.outerHost = outerHost;
    }
}
