package com.hall.entity.goldfield;

/**
 * 金币场，不一定真的消耗金币，可能是其他货币
 */
public class GoldField {
    private String id;
    private String name;
    private int minGold;
    private int maxGold;
    private String currency;
    /**
     * 底分，就是一盘最少消耗的货币数量
     */
    private int difen;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinGold() {
        return minGold;
    }

    public void setMinGold(int minGold) {
        this.minGold = minGold;
    }

    public int getMaxGold() {
        return maxGold;
    }

    public void setMaxGold(int maxGold) {
        this.maxGold = maxGold;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getDifen() {
        return difen;
    }

    public void setDifen(int difen) {
        this.difen = difen;
    }
}
