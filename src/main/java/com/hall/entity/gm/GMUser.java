package com.hall.entity.gm;

import dml.adminuser.entity.AdminUserBase;

public class GMUser extends AdminUserBase {
    private String account;
    private String name;
    private long createTime;

    public String getAccount() {
        return account;
    }

    @Override
    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getPassword() {
        return password;
    }
}
