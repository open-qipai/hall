package com.hall.entity.gm;

import dml.rbac.entity.Permission;

public class GMPermission implements Permission {
    private String id;
    private String name;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
