package com.hall.entity.gm;

import dml.adminuser.entity.AdminUserSession;

public class GMUserSession implements AdminUserSession {
    private String id;
    private String account;

    public GMUserSession() {
    }

    public GMUserSession(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getAccount() {
        return account;
    }

    @Override
    public void setAccount(String account) {
        this.account = account;
    }
}
