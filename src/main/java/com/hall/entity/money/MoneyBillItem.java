package com.hall.entity.money;

import dml.gamecurrency.entity.GameCurrencyAccountBillItemBase;

public class MoneyBillItem extends GameCurrencyAccountBillItemBase {
    private long id;

    private String reason;

    public MoneyBillItem() {
    }

    public MoneyBillItem(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public void setId(Object id) {
        this.id = (long) id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
