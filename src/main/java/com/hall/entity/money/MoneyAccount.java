package com.hall.entity.money;

import dml.gamecurrency.entity.GameCurrencyAccountBase;

public class MoneyAccount extends GameCurrencyAccountBase {

    private long id;

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public void setId(Object id) {
        this.id = (long) id;
    }
}
