package com.hall.entity.gameplay;

import dml.gamehallgameplay.entity.GamePlay;

import java.util.List;

public class HallGamePlay implements GamePlay {
    private long id;
    private GameMode gameMode;
    private String gameId;
    private String gameServerInnerHost;
    private String gameServerOuterHost;
    private String gameServerGameId;
    private List<Long> playerIdList;
    private int difen;

    public void playGame(GameMode gameMode, String gameId, String gameServerGameId,
                         String gameServerInnerHost, String gameServerOuterHost,
                         List<Long> playerIdList, int difen) {
        this.gameMode = gameMode;
        this.gameId = gameId;
        this.gameServerGameId = gameServerGameId;
        this.gameServerInnerHost = gameServerInnerHost;
        this.gameServerOuterHost = gameServerOuterHost;
        this.playerIdList = playerIdList;
        this.difen = difen;
    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public void setId(Object o) {
        this.id = (long) o;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameServerInnerHost() {
        return gameServerInnerHost;
    }

    public void setGameServerInnerHost(String gameServerInnerHost) {
        this.gameServerInnerHost = gameServerInnerHost;
    }

    public String getGameServerOuterHost() {
        return gameServerOuterHost;
    }

    public void setGameServerOuterHost(String gameServerOuterHost) {
        this.gameServerOuterHost = gameServerOuterHost;
    }

    public String getGameServerGameId() {
        return gameServerGameId;
    }

    public void setGameServerGameId(String gameServerGameId) {
        this.gameServerGameId = gameServerGameId;
    }

    public List<Object> getPlayerIdList() {
        return (List) playerIdList;
    }

    public void setPlayerIdList(List<Long> playerIdList) {
        this.playerIdList = playerIdList;
    }

    public int getDifen() {
        return difen;
    }

    public void setDifen(int difen) {
        this.difen = difen;
    }
}
