package com.hall.repository;

import dml.common.repository.CommonSingletonRepository;
import dml.id.entity.IdGenerator;

public interface UserSerialNumberGeneratorRepository extends CommonSingletonRepository<IdGenerator<String>> {
}
