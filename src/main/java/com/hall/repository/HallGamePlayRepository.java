package com.hall.repository;

import com.hall.entity.gameplay.HallGamePlay;
import dml.gamehallgameplay.repository.GamePlayRepository;

public interface HallGamePlayRepository extends GamePlayRepository<HallGamePlay, Long> {
}
