package com.hall.repository;

import dml.rbac.repository.UserRoleCleanupTaskRepository;

import java.util.List;

public interface GmUserRoleCleanupTaskRepository extends UserRoleCleanupTaskRepository {
    List<String> listAllName();
}
