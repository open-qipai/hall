package com.hall.repository;

import com.hall.entity.gm.GMUserSession;
import dml.adminuser.repository.AdminUserSessionRepository;

import java.util.List;

public interface GMUserSessionRepository extends AdminUserSessionRepository<GMUserSession> {
    List<String> getAllSessionId();
}
