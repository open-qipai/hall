package com.hall.repository;

import dml.user.entity.OpenIDUserBind;
import dml.user.repository.OpenIDUserBindRepository;

import java.util.List;

public interface HallOpenIDUserBindRepository extends OpenIDUserBindRepository {
    List<OpenIDUserBind> findByUserId(long userId);
}
