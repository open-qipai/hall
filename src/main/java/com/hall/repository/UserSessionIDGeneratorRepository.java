package com.hall.repository;

import dml.common.repository.CommonSingletonRepository;
import dml.id.entity.IdGenerator;

public interface UserSessionIDGeneratorRepository extends CommonSingletonRepository<IdGenerator<String>> {
}
