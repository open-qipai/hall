package com.hall.repository;

import dml.common.repository.CommonSingletonRepository;
import dml.id.entity.IdGenerator;

public interface HallGamePlayIDGeneratorRepository extends CommonSingletonRepository<IdGenerator<Long>> {
}
