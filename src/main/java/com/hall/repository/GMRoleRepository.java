package com.hall.repository;

import com.hall.entity.gm.GMRole;
import dml.rbac.repository.RoleRepository;

import java.util.List;

public interface GMRoleRepository extends RoleRepository<GMRole> {
    List<GMRole> list(int from, int size);

    int countAll();

    List<GMRole> listAll();

    List<String> listAllId();
}
