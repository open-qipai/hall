package com.hall.repository;

import com.hall.entity.user.HallUser;
import dml.user.repository.UserRepository;

import java.util.List;

public interface HallUserRepository extends UserRepository<HallUser, Long> {
    List<Long> getAllUserID();

    List<HallUser> findList(List<Long> userIdList);
}
