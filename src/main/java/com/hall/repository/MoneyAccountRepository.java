package com.hall.repository;

import com.hall.entity.money.MoneyAccount;
import dml.gamecurrency.repository.GameCurrencyAccountRepository;

import java.util.List;

public interface MoneyAccountRepository extends GameCurrencyAccountRepository<MoneyAccount, Long> {
    List<MoneyAccount> findMoneyAccountByIds(List idList);
}
