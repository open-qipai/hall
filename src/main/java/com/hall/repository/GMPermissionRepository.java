package com.hall.repository;

import com.hall.entity.gm.GMPermission;
import dml.rbac.repository.PermissionRepository;

import java.util.List;

public interface GMPermissionRepository extends PermissionRepository<GMPermission> {
    List<GMPermission> list(int from, int size);

    int countAll();

    List<GMPermission> listAll();
}
