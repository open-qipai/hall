package com.hall.repository;

import com.hall.entity.gm.GMUser;
import dml.adminuser.repository.AdminUserRepository;

import java.util.List;

public interface GMUserRepository extends AdminUserRepository<GMUser> {
    List<GMUser> getGMUserList(String accountSearch, String nameSearch, int from, int size);

    int getGMUserCount(String accountSearch, String nameSearch);

    List<String> listAllId();
}
