package com.hall.repository;

import com.hall.entity.money.MoneyBillItem;
import dml.gamecurrency.repository.GameCurrencyAccountBillItemRepository;

public interface MoneyBillItemRepository extends GameCurrencyAccountBillItemRepository<MoneyBillItem, Long> {
}
