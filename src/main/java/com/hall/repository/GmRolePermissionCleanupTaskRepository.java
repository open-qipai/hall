package com.hall.repository;

import dml.rbac.repository.RolePermissionCleanupTaskRepository;

import java.util.List;

public interface GmRolePermissionCleanupTaskRepository extends RolePermissionCleanupTaskRepository {
    List<String> listAllName();
}
