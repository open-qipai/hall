package com.hall.repository;

import dml.gamecurrency.entity.GameUserCurrencyAccounts;
import dml.gamecurrency.repository.GameUserCurrencyAccountsRepository;

import java.util.List;

public interface HallGameUserCurrencyAccountsRepository extends GameUserCurrencyAccountsRepository {
    List<GameUserCurrencyAccounts> findGameUserCurrencyAccountsByUserIds(List<Long> userIdList);
}
