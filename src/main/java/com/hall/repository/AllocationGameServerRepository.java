package com.hall.repository;

import com.hall.entity.allocation.AllocationGameServer;
import dml.gameserverallocation.loadbased.repository.LoadBalancedAllocationGameServerRepository;

import java.util.List;

public interface AllocationGameServerRepository extends LoadBalancedAllocationGameServerRepository<AllocationGameServer> {
    List<AllocationGameServer> getAllForGame(String gameId);
}
