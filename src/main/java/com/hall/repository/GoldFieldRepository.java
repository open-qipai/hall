package com.hall.repository;

import com.hall.entity.goldfield.GoldField;
import dml.common.repository.CommonRepository;

import java.util.List;

public interface GoldFieldRepository extends CommonRepository<GoldField, String> {
    List<GoldField> list(int from, int size);

    int countAll();
}
