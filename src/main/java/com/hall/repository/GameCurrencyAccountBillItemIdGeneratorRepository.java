package com.hall.repository;

import dml.common.repository.CommonSingletonRepository;
import dml.id.entity.IdGenerator;

public interface GameCurrencyAccountBillItemIdGeneratorRepository extends CommonSingletonRepository<IdGenerator<Long>> {
}
