package com.hall.repository;

import com.hall.entity.user.HallUserSession;
import dml.user.repository.UserSessionRepository;

import java.util.List;

public interface HallUserSessionRepository extends UserSessionRepository<HallUserSession> {
    List<String> getAllSessionId();

    int count();
}
