package com.hall.repository;

import dml.gamematch.repository.RandomMatchPlayerRepository;

import java.util.List;

public interface GoldFieldRandomMatchPlayerRepository extends RandomMatchPlayerRepository {
    List<String> getAllMatchPlayerIds(String goldFieldId);
}
