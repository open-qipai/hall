package com.hall.repository;

import com.hall.entity.gm.GMApi;
import dml.common.repository.CommonRepository;

import java.util.List;

public interface GMApiRepository extends CommonRepository<GMApi, String> {
    List<GMApi> queryAll(String uri, int from, int size);

    int count(String uri);
}
