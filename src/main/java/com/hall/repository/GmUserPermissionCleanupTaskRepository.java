package com.hall.repository;

import dml.rbac.repository.UserPermissionCleanupTaskRepository;

import java.util.List;

public interface GmUserPermissionCleanupTaskRepository extends UserPermissionCleanupTaskRepository {
    List<String> listAllName();
}
