package com.hall.repository.impl;

import com.hall.entity.gm.GMApi;
import com.hall.repository.GMApiRepository;
import erp.mongodb.MongodbRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class MongodbGMApiRepository extends MongodbRepository<GMApi, String> implements GMApiRepository {
    public MongodbGMApiRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<GMApi> queryAll(String uri, int from, int size) {
        //uri是模糊查询条件，from和size是分页查询条件
        Query query = new Query();
        query.skip(from);
        query.limit(size);
        query.addCriteria(where("uri").regex(uri));
        return mongoTemplate.find(query, GMApi.class, collectionName);
    }

    @Override
    public int count(String uri) {
        Query query = new Query();
        query.addCriteria(where("uri").regex(uri));
        return (int) mongoTemplate.count(query, GMApi.class, collectionName);
    }
}
