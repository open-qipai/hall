package com.hall.repository.impl;

import com.hall.entity.goldfield.GoldField;
import com.hall.repository.GoldFieldRepository;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbGoldFieldRepository extends MongodbRepository<GoldField, String> implements GoldFieldRepository {

    @Autowired
    public MongodbGoldFieldRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<GoldField> list(int from, int size) {
        Query query = new Query();
        query.skip(from).limit(size);
        return mongoTemplate.find(query, GoldField.class, collectionName);
    }

    @Override
    public int countAll() {
        return (int) count();
    }


}
