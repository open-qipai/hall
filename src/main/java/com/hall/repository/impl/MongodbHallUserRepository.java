package com.hall.repository.impl;

import com.hall.entity.user.HallUser;
import com.hall.repository.HallUserRepository;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbHallUserRepository extends MongodbRepository<HallUser, Long> implements HallUserRepository {

    @Autowired
    public MongodbHallUserRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<Long> getAllUserID() {
        return queryAllIds();
    }

    @Override
    public List<HallUser> findList(List<Long> userIdList) {
        Query query = new Query(Criteria.where("id").in(userIdList));
        return mongoTemplate.find(query, HallUser.class, collectionName);
    }
}
