package com.hall.repository.impl;

import com.hall.entity.gm.GMUserSession;
import com.hall.repository.GMUserSessionRepository;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisGMUserSessionRepository extends AllIdQuerySupportedRedisRepository<GMUserSession, String>
        implements GMUserSessionRepository {

    public RedisGMUserSessionRepository(RedisTemplate<String, Object> redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public List<String> getAllSessionId() {
        return queryAllIds();
    }
}
