package com.hall.repository.impl;

import com.hall.entity.allocation.AllocationGameServer;
import com.hall.repository.AllocationGameServerRepository;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RedisAllocationGameServerRepository extends AllIdQuerySupportedRedisRepository<AllocationGameServer, String>
        implements AllocationGameServerRepository {

    @Autowired
    public RedisAllocationGameServerRepository(RedisTemplate redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public List<AllocationGameServer> getAllForGame(String gameId) {
        List<String> allIds = queryAllIds();
        List<AllocationGameServer> all = new ArrayList<>();
        for (String id : allIds) {
            AllocationGameServer allocationGameServer = find(id);
            if (allocationGameServer.getGameId().equals(gameId)) {
                all.add(allocationGameServer);
            }
        }
        return all;
    }
}
