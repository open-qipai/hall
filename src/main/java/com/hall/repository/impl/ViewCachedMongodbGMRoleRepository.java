package com.hall.repository.impl;

import com.hall.entity.gm.GMRole;
import com.hall.repository.GMRoleRepository;
import erp.mongodb.MongodbRepository;
import erp.viewcached.ViewCachedRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class ViewCachedMongodbGMRoleRepository extends ViewCachedRepository<GMRole, String> implements GMRoleRepository {

    private MongoTemplate mongoTemplate;
    private MongodbRepository<GMRole, String> mongodbRepository;

    public ViewCachedMongodbGMRoleRepository(MongoTemplate mongoTemplate) {
        super(new MongodbRepository<>(mongoTemplate, GMRole.class));
        this.mongoTemplate = mongoTemplate;
        this.mongodbRepository = (MongodbRepository<GMRole, String>) underlyingRepository;
    }

    @Override
    public List<GMRole> list(int from, int size) {
        Query query = new Query();
        query.skip(from).limit(size);
        return mongoTemplate.find(query, GMRole.class, mongodbRepository.getCollectionName());
    }

    @Override
    public int countAll() {
        return (int) mongodbRepository.count();
    }

    @Override
    public List<GMRole> listAll() {
        return mongodbRepository.queryAllEntities();
    }

    @Override
    public List<String> listAllId() {
        return mongodbRepository.queryAllIds();
    }
}
