package com.hall.repository.impl;

import com.hall.repository.GoldFieldRandomMatchPlayerRepository;
import dml.gamematch.entity.randommatch.RandomMatchPlayer;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbGoldFieldRandomMatchPlayerRepository extends MongodbRepository<RandomMatchPlayer, String> implements GoldFieldRandomMatchPlayerRepository {

    @Autowired
    public MongodbGoldFieldRandomMatchPlayerRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate, "GoldFieldRandomMatchPlayerRepository");
        IndexOperations indexOps = mongoTemplate.indexOps(collectionName);
        Index index = (new Index()).on("matchId", Sort.Direction.ASC);
        indexOps.ensureIndex(index);
    }

    @Override
    public List<String> getAllMatchPlayerIds(String goldFieldId) {
        Query query = Query.query(Criteria.where("matchId").is(goldFieldId));
        query.fields().include("playerId");
        return mongoTemplate.find(query, RandomMatchPlayer.class, collectionName)
                .stream().map(RandomMatchPlayer::getPlayerId).toList();
    }
}
