package com.hall.repository.impl;

import com.hall.repository.HallOpenIDUserBindRepository;
import dml.user.entity.OpenIDUserBind;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbHallOpenIDUserBindRepository extends MongodbRepository<OpenIDUserBind, String> implements HallOpenIDUserBindRepository {

    @Autowired
    public MongodbHallOpenIDUserBindRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate, "hallOpenIDUserBindRepository");
    }

    @Override
    public List<OpenIDUserBind> findByUserId(long userId) {
        return queryAllByField("userID", userId);
    }
}
