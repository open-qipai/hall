package com.hall.repository.impl;

import com.hall.entity.money.MoneyAccount;
import com.hall.repository.MoneyAccountRepository;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbMoneyAccountRepository extends MongodbRepository<MoneyAccount, Long> implements MoneyAccountRepository {
    @Autowired
    public MongodbMoneyAccountRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<MoneyAccount> findMoneyAccountByIds(List idList) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").in(idList));
        return mongoTemplate.find(query, MoneyAccount.class, collectionName);
    }
}
