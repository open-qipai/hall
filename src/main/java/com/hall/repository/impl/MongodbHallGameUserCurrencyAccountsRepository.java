package com.hall.repository.impl;

import com.hall.repository.HallGameUserCurrencyAccountsRepository;
import dml.gamecurrency.entity.GameUserCurrencyAccounts;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbHallGameUserCurrencyAccountsRepository extends MongodbRepository<GameUserCurrencyAccounts, Object>
        implements HallGameUserCurrencyAccountsRepository {
    @Autowired
    public MongodbHallGameUserCurrencyAccountsRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<GameUserCurrencyAccounts> findGameUserCurrencyAccountsByUserIds(List<Long> userIdList) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").in(userIdList));
        return mongoTemplate.find(query, GameUserCurrencyAccounts.class, collectionName);
    }
}
