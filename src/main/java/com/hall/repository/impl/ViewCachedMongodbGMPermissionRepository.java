package com.hall.repository.impl;

import com.hall.entity.gm.GMPermission;
import com.hall.repository.GMPermissionRepository;
import erp.mongodb.MongodbRepository;
import erp.viewcached.ViewCachedRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class ViewCachedMongodbGMPermissionRepository extends ViewCachedRepository<GMPermission, String> implements GMPermissionRepository {

    private MongoTemplate mongoTemplate;
    private MongodbRepository<GMPermission, String> mongodbRepository;

    public ViewCachedMongodbGMPermissionRepository(MongoTemplate mongoTemplate) {
        super(new MongodbRepository<>(mongoTemplate, GMPermission.class));
        this.mongoTemplate = mongoTemplate;
        this.mongodbRepository = (MongodbRepository<GMPermission, String>) underlyingRepository;
    }

    @Override
    public List<GMPermission> list(int from, int size) {
        Query query = new Query();
        query.skip(from).limit(size);
        return mongoTemplate.find(query, GMPermission.class, mongodbRepository.getCollectionName());
    }

    @Override
    public int countAll() {
        return (int) mongodbRepository.count();
    }

    @Override
    public List<GMPermission> listAll() {
        return mongodbRepository.queryAllEntities();
    }
}
