package com.hall.repository.impl;

import com.hall.entity.user.HallUser;
import com.hall.repository.HallUserRepository;
import erp.repository.impl.mem.MemRepository;

import java.util.List;

//@Component
public class MemHallUserRepository extends MemRepository<HallUser, Long> implements HallUserRepository {
    @Override
    public List<Long> getAllUserID() {
        return queryAllIds();
    }

    @Override
    public List<HallUser> findList(List<Long> userIdList) {
        return List.of();
    }
}
