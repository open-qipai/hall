package com.hall.repository.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import erp.process.definition.Process;
import erp.viewcached.ViewCachedUpdater;
import org.springframework.stereotype.Component;

@Component
public class ViewCachedUpdaterImpl extends ViewCachedUpdater {


    @Override
    protected Process parseProcessFromJson(String s) {
        return JSON.parseObject(s, Process.class, JSONReader.Feature.SupportAutoType);
    }
}
