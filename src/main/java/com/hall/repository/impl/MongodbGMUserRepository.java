package com.hall.repository.impl;

import com.hall.entity.gm.GMUser;
import com.hall.repository.GMUserRepository;
import erp.mongodb.MongodbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongodbGMUserRepository extends MongodbRepository<GMUser, String> implements GMUserRepository {

    @Autowired
    public MongodbGMUserRepository(MongoTemplate mongoTemplate) {
        super(mongoTemplate);
    }

    @Override
    public List<GMUser> getGMUserList(String accountSearch, String nameSearch, int from, int size) {
        //通过mongoTemplate查询从from开始的size条数据, accountSearch,nameSearch为模糊查询条件, 为空则忽略条件, 以createTime降序排序
        Query query = getQuery(accountSearch, nameSearch);
        query.skip(from).limit(size).with(Sort.by(Sort.Order.desc("createTime")));
        return mongoTemplate.find(query, GMUser.class, collectionName);
    }

    @Override
    public int getGMUserCount(String accountSearch, String nameSearch) {
        //通过mongoTemplate查询符合条件的数据总数, account,name为模糊查询条件, 为空则忽略条件
        Query query = getQuery(accountSearch, nameSearch);
        return (int) mongoTemplate.count(query, GMUser.class, collectionName);
    }

    @Override
    public List<String> listAllId() {
        return queryAllIds();
    }

    private Query getQuery(String account, String name) {
        Query query = new Query();
        if (account != null && !account.isEmpty()) {
            query.addCriteria(Criteria.where("account").regex(account));
        }
        if (name != null && !name.isEmpty()) {
            query.addCriteria(Criteria.where("name").regex(name));
        }
        return query;
    }

}
