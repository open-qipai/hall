package com.hall.repository.impl;

import com.hall.repository.GmRolePermissionCleanupTaskRepository;
import dml.rbac.entity.cleanup.RolePermissionCleanupTask;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisGmRolePermissionCleanupTaskRepository extends AllIdQuerySupportedRedisRepository<RolePermissionCleanupTask, String>
        implements GmRolePermissionCleanupTaskRepository {
    public RedisGmRolePermissionCleanupTaskRepository(RedisTemplate<String, Object> redisTemplate) {
        super(redisTemplate, "GmRolePermissionCleanupTaskRepository");
    }

    @Override
    public List<String> listAllName() {
        return queryAllIds();
    }
}
