package com.hall.repository.impl;

import com.hall.repository.GmUserPermissionCleanupTaskRepository;
import dml.rbac.entity.cleanup.UserPermissionCleanupTask;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisGmUserPermissionCleanupTaskRepository extends AllIdQuerySupportedRedisRepository<UserPermissionCleanupTask, String>
        implements GmUserPermissionCleanupTaskRepository {
    public RedisGmUserPermissionCleanupTaskRepository(RedisTemplate<String, Object> redisTemplate) {
        super(redisTemplate, "GmUserPermissionCleanupTaskRepository");
    }

    @Override
    public List<String> listAllName() {
        return queryAllIds();
    }
}
