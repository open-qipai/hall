package com.hall.repository.impl;

import com.hall.entity.user.HallUserSession;
import com.hall.repository.HallUserSessionRepository;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RedisHallUserSessionRepository extends AllIdQuerySupportedRedisRepository<HallUserSession, String>
        implements HallUserSessionRepository {

    @Autowired
    public RedisHallUserSessionRepository(RedisTemplate redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public List<String> getAllSessionId() {
        return queryAllIds();
    }

    @Override
    public int count() {
        return queryAllIds().size();
    }
}
