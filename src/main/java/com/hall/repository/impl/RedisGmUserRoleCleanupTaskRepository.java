package com.hall.repository.impl;

import com.hall.repository.GmUserRoleCleanupTaskRepository;
import dml.rbac.entity.cleanup.UserRoleCleanupTask;
import erp.redis.AllIdQuerySupportedRedisRepository;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisGmUserRoleCleanupTaskRepository extends AllIdQuerySupportedRedisRepository<UserRoleCleanupTask, String>
        implements GmUserRoleCleanupTaskRepository {
    public RedisGmUserRoleCleanupTaskRepository(RedisTemplate<String, Object> redisTemplate) {
        super(redisTemplate, "GmUserRoleCleanupTaskRepository");
    }

    @Override
    public List<String> listAllName() {
        return queryAllIds();
    }
}
