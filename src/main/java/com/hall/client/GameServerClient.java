package com.hall.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hall.client.request.StartGameRequest;
import com.hall.web.viewobject.CommonVO;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;

@Component
public class GameServerClient {

    private HttpClient httpClient = HttpClient.newHttpClient();
    private ObjectMapper objectMapper = new ObjectMapper();

    public String startGame(String innerHost,
                            List<String> matchedPlayerIdStrList,
                            boolean shaozhongfa) {
        try {
            URI uri = new URI("http://" + innerHost + "/game/start_game");
            String requestBody = objectMapper.writeValueAsString(new StartGameRequest(shaozhongfa, matchedPlayerIdStrList));
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 200) {
                CommonVO commonVO = objectMapper.readValue(response.body(), CommonVO.class);
                return ((Map) commonVO.getData()).get("gameId").toString();
            } else {
                throw new RuntimeException("Failed to create game, status code: " + response.statusCode());
            }
        } catch (Exception e) {
            throw new RuntimeException("Exception occurred while creating game", e);
        }
    }

    public void bindGamePlayId(String innerHost, String gameServerGameId, String gamePlayId) {
        try {
            URI uri = new URI("http://" + innerHost + "/game/bind_game_play_id");
            String requestBody = objectMapper.writeValueAsString(Map.of("gameServerGameId", gameServerGameId, "gamePlayId", gamePlayId));
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() != 200) {
                throw new RuntimeException("Failed to bind game play id, status code: " + response.statusCode());
            }
        } catch (Exception e) {
            throw new RuntimeException("Exception occurred while binding game play id", e);
        }
    }
}
