package com.hall.client.request;

import java.util.ArrayList;
import java.util.List;

public class PlayerRO {
    private String userId;

    public PlayerRO() {
    }

    public PlayerRO(String userId) {
        this.userId = userId;
    }

    public static List<PlayerRO> fromHallUsers(List<String> playerIdStrList) {
        List<PlayerRO> playerROs = new ArrayList<>();
        for (String hallUserId : playerIdStrList) {
            playerROs.add(new PlayerRO(hallUserId));
        }
        return playerROs;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
