package com.hall.client.request;

import java.util.List;

public class StartGameRequest {
    private boolean shaozhongfa;
    private List<PlayerRO> players;

    public StartGameRequest(boolean shaozhongfa, List<String> playerIdStrList) {
        this.shaozhongfa = shaozhongfa;
        this.players = PlayerRO.fromHallUsers(playerIdStrList);
    }

    public boolean isShaozhongfa() {
        return shaozhongfa;
    }

    public void setShaozhongfa(boolean shaozhongfa) {
        this.shaozhongfa = shaozhongfa;
    }

    public List<PlayerRO> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerRO> players) {
        this.players = players;
    }
}
