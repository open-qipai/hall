package com.hall;

import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.gm.GMUser;
import com.hall.entity.money.MoneyBillItem;
import com.hall.repository.GameCurrencyAccountBillItemIdGeneratorRepository;
import com.hall.repository.HallGamePlayRepository;
import com.hall.repository.MoneyBillItemRepository;
import com.hall.repository.UserSessionIDGeneratorRepository;
import com.hall.service.gm.GMUserService;
import dml.gamecurrency.repository.GameCurrencyAccountIdGeneratorRepository;
import dml.gamematch.entity.MatchState;
import dml.gamematch.entity.PlayerCurrentMatch;
import dml.gamematch.entity.failureacquisition.PlayerMatchFailureAcquisitionTask;
import dml.gamematch.entity.failureacquisition.PlayerMatchFailureAcquisitionTaskSegment;
import dml.gamematch.entity.resultacquisition.MatchResultAcquisitionTask;
import dml.gamematch.entity.resultacquisition.MatchResultAcquisitionTaskSegment;
import dml.gamematch.repository.*;
import dml.id.entity.SnowflakeIdGenerator;
import dml.id.entity.SnowflakeStringIdGenerator;
import dml.id.entity.UUIDStyleRandomStringIdGenerator;
import dml.user.entity.UserCurrentSession;
import dml.user.entity.UserSessionAliveKeeper;
import dml.user.repository.UserCurrentSessionRepository;
import dml.user.repository.UserIDGeneratorRepository;
import dml.user.repository.UserSessionAliveKeeperRepository;
import erp.ERP;
import erp.mongodb.MongodbRepository;
import erp.redis.ERPRedis;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableKafka
public class HallApp {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;

    public static void main(String[] args) {
        ERP.useAnnotation();
        ERPRedis.usePipelineAnnotation();
        ApplicationContext context = SpringApplication.run(HallApp.class, args);

        //检查并添加默认管理员
        GMUserService gmUserService = context.getBean(GMUserService.class);
        GMUser gmUser = gmUserService.getGMUserByAccount("admin");
        if (gmUser == null) {
            gmUserService.addGMUser("admin", "admin", "初始管理员", System.currentTimeMillis());
        }
    }

    @Bean
    public UserIDGeneratorRepository userIDGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(UserIDGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeIdGenerator(1L), "userIDGeneratorRepository"));
    }

    @Bean
    public UserSessionIDGeneratorRepository userSessionIDGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(UserSessionIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDStyleRandomStringIdGenerator(), "userSessionIDGeneratorRepository"));
    }

    @Bean
    public UserSessionAliveKeeperRepository hallUserSessionAliveKeeperRepository() {
        return RepositoryFactory.newInstance(UserSessionAliveKeeperRepository.class,
                new RedisRepository(redisTemplate, UserSessionAliveKeeper.class, "hallUserSessionAliveKeeperRepository"));
    }

    @Bean
    public UserCurrentSessionRepository hallUserCurrentSessionRepository() {
        return RepositoryFactory.newInstance(UserCurrentSessionRepository.class,
                new RedisRepository(redisTemplate, UserCurrentSession.class, "hallUserCurrentSessionRepository"));
//                new MemRepository(UserCurrentSession.class, "hallUserCurrentSessionRepository"));
    }


    @Bean
    public GameCurrencyAccountIdGeneratorRepository gameCurrencyAccountIdGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(GameCurrencyAccountIdGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeIdGenerator(1L), "gameCurrencyAccountIdGeneratorRepository"));
    }

    @Bean
    public MoneyBillItemRepository moneyBillItemRepository() {
        return RepositoryFactory.newInstance(MoneyBillItemRepository.class,
                new MongodbRepository(mongoTemplate, MoneyBillItem.class));
    }

    @Bean
    public GameCurrencyAccountBillItemIdGeneratorRepository gameCurrencyAccountBillItemIdGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(GameCurrencyAccountBillItemIdGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeIdGenerator(1L), "gameCurrencyAccountBillItemIdGeneratorRepository"));
    }

    @Bean
    public MatchStateRepository goldFieldMatchStateRepository() {
        return RepositoryFactory.newInstance(MatchStateRepository.class,
                new MongodbRepository(mongoTemplate, MatchState.class, "goldFieldMatchStateRepository"));
    }

    @Bean
    public PlayerCurrentMatchRepository playerCurrentMatchRepository() {
        return RepositoryFactory.newInstance(PlayerCurrentMatchRepository.class,
                new RedisRepository(redisTemplate, PlayerCurrentMatch.class, "goldFieldPlayerCurrentMatchRepository"));
    }

    @Bean
    public MatchResultAcquisitionTaskRepository goldFieldMatchResultAcquisitionTaskRepository() {
        return RepositoryFactory.newInstance(MatchResultAcquisitionTaskRepository.class,
                new MongodbRepository(mongoTemplate, MatchResultAcquisitionTask.class, "goldFieldMatchResultAcquisitionTaskRepository"));
    }

    @Bean
    public MatchResultAcquisitionTaskSegmentRepository goldFieldMatchResultAcquisitionTaskSegmentRepository() {
        return RepositoryFactory.newInstance(MatchResultAcquisitionTaskSegmentRepository.class,
                new MongodbRepository(mongoTemplate, MatchResultAcquisitionTaskSegment.class, "goldFieldMatchResultAcquisitionTaskSegmentRepository"));
    }

    @Bean
    public MatchResultAcquisitionTaskSegmentIDGeneratorRepository goldFieldMatchResultAcquisitionTaskSegmentIDGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(MatchResultAcquisitionTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeStringIdGenerator(1L), "goldFieldMatchResultAcquisitionTaskSegmentIDGeneratorRepository"));
    }

    @Bean
    public PlayerMatchFailureAcquisitionTaskRepository goldFieldPlayerMatchFailureAcquisitionTaskRepository() {
        return RepositoryFactory.newInstance(PlayerMatchFailureAcquisitionTaskRepository.class,
                new MongodbRepository(mongoTemplate, PlayerMatchFailureAcquisitionTask.class, "goldFieldPlayerMatchFailureAcquisitionTaskRepository"));
    }

    @Bean
    public PlayerMatchFailureAcquisitionTaskSegmentRepository goldFieldPlayerMatchFailureAcquisitionTaskSegmentRepository() {
        return RepositoryFactory.newInstance(PlayerMatchFailureAcquisitionTaskSegmentRepository.class,
                new MongodbRepository(mongoTemplate, PlayerMatchFailureAcquisitionTaskSegment.class, "goldFieldPlayerMatchFailureAcquisitionTaskSegmentRepository"));
    }

    @Bean
    public PlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository goldFieldPlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository() {
        return SingletonRepositoryFactory.newInstance(PlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeStringIdGenerator(1L), "goldFieldPlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository"));
    }

    @Bean
    public HallGamePlayRepository hallGamePlayRepository() {
        return RepositoryFactory.newInstance(HallGamePlayRepository.class,
                new RedisRepository(redisTemplate, HallGamePlay.class));
    }


}
