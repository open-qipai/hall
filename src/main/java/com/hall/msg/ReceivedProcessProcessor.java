package com.hall.msg;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.hall.entity.gm.GMPermission;
import com.hall.entity.gm.GMRole;
import com.hall.entity.user.HallUser;
import com.hall.service.MoneyService;
import com.hall.service.gm.GMRbacService;
import com.hall.service.result.BatchRegisterUsersResult;
import com.hall.service.result.LoginResult;
import com.hall.web.ws.WsNotifier;
import dml.user.service.result.LoginByOpenIDResult;
import erp.ERP;
import erp.process.definition.Process;
import erp.process.definition.TypedEntity;
import erp.process.definition.TypedResult;
import erp.viewcached.ViewCachedUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ReceivedProcessProcessor {

    @Autowired
    private ViewCachedUpdater viewCachedUpdater;

    @Autowired
    private MoneyService moneyService;

    @Autowired
    private GMRbacService gmRbacService;

    @Autowired
    private WsNotifier wsNotifier;

    public void process(Process process) {
        updateViewCached(process);

        clearRolePermissions(process);
        clearUserRoles(process);
        clearUserPermissions(process);
        //TODO 用户删除后删除用户权限，用户角色

        if (process.getName().equals("com.hall.service.LoginService.doLogin")) {
            sendNewUserInitGold(process);
        } else if (process.getName().equals("com.hall.service.LoginService.batchRegisterUsers")) {
            sendBatchRegisterUsersInitGold(process);
        }

    }

    private void sendNewUserInitGold(Process process) {
        TypedResult typedResult = process.getResult();
        LoginResult loginResult = (LoginResult) typedResult.getResult();
        LoginByOpenIDResult loginByOpenIDResult = loginResult.getLoginByOpenIDResult();
        if (loginByOpenIDResult.isCreateNewUser()) {
            ERP.retry(() -> moneyService.sendGold((Long) loginByOpenIDResult.getNewUserSession().getUserID(), 20000, "新用户注册赠送"), 16, 10);
            wsNotifier.notifyUserMoneyChanged((Long) loginByOpenIDResult.getNewUserSession().getUserID());
        }
    }

    private void sendBatchRegisterUsersInitGold(Process process) {
        TypedResult typedResult = process.getResult();
        BatchRegisterUsersResult batchRegisterUsersResult = (BatchRegisterUsersResult) typedResult.getResult();
        Map<String, HallUser> openIdToHallUserMap = batchRegisterUsersResult.getOpenIdToHallUserMap();
        for (Map.Entry<String, HallUser> entry : openIdToHallUserMap.entrySet()) {
            HallUser hallUser = entry.getValue();
            moneyService.sendGold((Long) hallUser.getId(), 20000, "新用户注册赠送");
        }
    }

    private void updateViewCached(Process process) {
        viewCachedUpdater.updateByProcessJson(JSON.toJSONString(process, JSONWriter.Feature.WriteClassName));
    }

    private void clearRolePermissions(Process process) {
        List<TypedEntity> deletedEntityList = process.getDeletedEntityList();
        for (TypedEntity typedEntity : deletedEntityList) {
            if (typedEntity.getType().equals(GMPermission.class.getName())) {
                GMPermission deletedGmPermission = (GMPermission) typedEntity.getEntity();
                gmRbacService.createRolePermissionCleanupTask(deletedGmPermission.getId(), System.currentTimeMillis());
            }
        }
    }

    private void clearUserRoles(Process process) {
        List<TypedEntity> deletedEntityList = process.getDeletedEntityList();
        for (TypedEntity typedEntity : deletedEntityList) {
            if (typedEntity.getType().equals(GMRole.class.getName())) {
                GMRole deletedGmRole = (GMRole) typedEntity.getEntity();
                gmRbacService.createUserRoleCleanupTask(deletedGmRole.getId(), System.currentTimeMillis());
            }
        }
    }

    private void clearUserPermissions(Process process) {
        List<TypedEntity> deletedEntityList = process.getDeletedEntityList();
        for (TypedEntity typedEntity : deletedEntityList) {
            if (typedEntity.getType().equals(GMPermission.class.getName())) {
                GMPermission deletedGmPermission = (GMPermission) typedEntity.getEntity();
                gmRbacService.createUserPermissionCleanupTask(deletedGmPermission.getId(), System.currentTimeMillis());
            }
        }
    }

}
