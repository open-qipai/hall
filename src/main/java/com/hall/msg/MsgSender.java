package com.hall.msg;

import com.hall.entity.goldfield.GoldField;

import java.util.List;

public interface MsgSender {
    public void sendProcess();

    void sendNotMatchedPlayers(GoldField goldField, List<String> notMatchedPlayers);
}
