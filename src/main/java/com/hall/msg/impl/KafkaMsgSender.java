package com.hall.msg.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.hall.entity.goldfield.GoldField;
import com.hall.msg.MsgSender;
import erp.ERP;
import erp.process.definition.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class KafkaMsgSender implements MsgSender {


    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void sendProcess() {
        Process process = ERP.getProcess();
        if (process == null) {
            return;
        }
        kafkaTemplate.send("process", JSON.toJSONString(process, JSONWriter.Feature.WriteClassName));
    }

    @Override
    public void sendNotMatchedPlayers(GoldField goldField, List<String> notMatchedPlayers) {
        if (notMatchedPlayers == null || notMatchedPlayers.isEmpty()) {
            return;
        }
        Map message = new HashMap();
        message.put("goldFieldId", goldField.getId());
        message.put("minGold", goldField.getMinGold() + "");
        message.put("maxGold", goldField.getMaxGold() + "");
        message.put("notMatchedPlayers", notMatchedPlayers);
        kafkaTemplate.send("not_matched_players", JSON.toJSONString(message));
    }
}
