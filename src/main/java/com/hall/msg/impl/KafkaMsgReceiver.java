package com.hall.msg.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import com.hall.msg.ReceivedProcessProcessor;
import erp.process.definition.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class KafkaMsgReceiver {

    //线程池
    ExecutorService threadPool = Executors.newCachedThreadPool();
    @Autowired
    private ReceivedProcessProcessor receivedProcessProcessor;

    @KafkaListener(topics = "process")
    public void listen(String message) {
        threadPool.execute(() -> {
            try {
                receivedProcessProcessor.process(JSON.parseObject(message, Process.class, JSONReader.Feature.SupportAutoType));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
