package com.hall.msg.impl;

import com.google.common.eventbus.EventBus;
import com.hall.entity.goldfield.GoldField;
import com.hall.msg.MsgSender;
import erp.ERP;
import erp.process.definition.Process;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zheng chengdong
 */
//@Component
public class EventBusMsgSender implements MsgSender {

    EventBus processBus;

    @Autowired
    public EventBusMsgSender(EventBusMsgReceiver receiver) {
        this.processBus = new EventBus("process");
        processBus.register(receiver);
    }

    public void sendProcess() {
        Process process = ERP.getProcess();
        if (process == null) {
            return;
        }
        processBus.post(process);
    }

    @Override
    public void sendNotMatchedPlayers(GoldField goldField, List<String> notMatchedPlayers) {

    }

}
