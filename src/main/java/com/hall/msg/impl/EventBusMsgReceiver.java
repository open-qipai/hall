package com.hall.msg.impl;

import com.google.common.eventbus.Subscribe;
import com.hall.msg.ReceivedProcessProcessor;
import erp.process.definition.Process;
import org.springframework.beans.factory.annotation.Autowired;

//@Component
public class EventBusMsgReceiver {

    @Autowired
    private ReceivedProcessProcessor receivedProcessProcessor;


    @Subscribe
    public void receiveProcess(Process process) throws InterruptedException {
        receivedProcessProcessor.process(process);
    }

}
