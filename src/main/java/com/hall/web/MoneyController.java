package com.hall.web;

import com.hall.entity.money.MoneyAccount;
import com.hall.service.MoneyService;
import com.hall.web.viewobject.CommonVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hall/money")
public class MoneyController {

    @Autowired
    private MoneyService moneyService;

    @PostMapping("/my_gold")
    @ResponseBody
    public CommonVO myGold(@RequestAttribute("userId") long userId) {
        int gold = moneyService.getGold(userId);
        return CommonVO.success("gold", gold);
    }

    @PostMapping("/my_money")
    @ResponseBody
    public CommonVO myMoney(@RequestAttribute("userId") long userId) {
        List<MoneyAccount> allMoneyAccount = moneyService.getAllMoneyAccountForUser(userId);
        CommonVO vo = CommonVO.success();
        if (allMoneyAccount == null) {
            return vo;
        }
        for (MoneyAccount moneyAccount : allMoneyAccount) {
            vo.append(moneyAccount.getCurrency(), moneyAccount.getBalance());
        }
        return vo;
    }
}
