package com.hall.web;

import com.hall.entity.gm.GMApi;
import com.hall.entity.gm.GMPermission;
import com.hall.entity.gm.GMRole;
import com.hall.entity.gm.GMUser;
import com.hall.entity.goldfield.GoldField;
import com.hall.msg.MsgSender;
import com.hall.service.GoldFieldService;
import com.hall.service.LoginService;
import com.hall.service.gm.GMApiService;
import com.hall.service.gm.GMRbacService;
import com.hall.service.gm.GMUserService;
import com.hall.web.request.*;
import com.hall.web.viewobject.*;
import dml.adminuser.service.result.LoginResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/gm")
public class GMController {

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private GMUserService gmUserService;

    @Autowired
    private GoldFieldService goldFieldService;

    @Autowired
    private GMRbacService gmRbacService;

    @Autowired
    private GMApiService gmApiService;

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    @ResponseBody
    public CommonVO login(@RequestBody GMLoginRequest gmLoginRequest) {
        String account = gmLoginRequest.getAccount();
        String password = gmLoginRequest.getPassword();
        LoginResult result = gmUserService.login(account, password);
        if (result.isSuccess()) {
            List<String> permissions = gmRbacService.getAllPermissions(account);
            return CommonVO.success("token", result.getNewSession().getId()).append("permissions", permissions);
        } else {
            if (result.isNoAccount()) {
                return CommonVO.unsuccess("无此账号");
            }
            if (result.isWrongPassword()) {
                return CommonVO.unsuccess("密码错误");
            }
            if (result.isBanned()) {
                return CommonVO.unsuccess("账号被锁定");
            }
            return CommonVO.unsuccess("未知错误");
        }
    }

    @PostMapping("/logout")
    @ResponseBody
    public CommonVO logout(@RequestAttribute("account") String account) {
        gmUserService.logout(account);
        return CommonVO.success();
    }

    @PostMapping("/gm_user_list")
    @ResponseBody
    public CommonVO gmUserList(@RequestBody GMUserListRequest gmUserListRequest) {
        String account = gmUserListRequest.getAccount();
        String name = gmUserListRequest.getName();
        int pageNum = gmUserListRequest.getPageNum();
        int pageSize = gmUserListRequest.getPageSize();
        int from = (pageNum - 1) * pageSize;
        List<GMUser> gmUserList = gmUserService.getGMUserList(account, name, from, pageSize);
        int total = gmUserService.getGMUserCount(account, name);
        return CommonVO.success(new DataListPageVO(GMUserVO.toVOList(gmUserList), total));
    }

    @PostMapping("/add_gm_user")
    @ResponseBody
    public CommonVO addGMUser(@RequestBody GMUserVO gmUserVO) {
        boolean exists = gmUserService.addGMUser(gmUserVO.getAccount(), gmUserVO.getPassword(), gmUserVO.getName(), System.currentTimeMillis());
        return CommonVO.success("exists", exists);
    }

    @PostMapping("/get_gm_user")
    @ResponseBody
    public CommonVO getGMUser(@RequestBody GetGMUserRequest getGMUserRequest) {
        GMUser gmUser = gmUserService.getGMUserByAccount(getGMUserRequest.getAccount());
        return CommonVO.success(new GMUserVO(gmUser));
    }

    @PostMapping("/update_gm_user")
    @ResponseBody
    public CommonVO updateGMUser(@RequestBody GMUserVO gmUserVO) {
        GMUser updatedGMUser = gmUserService.updateGMUser(gmUserVO.getAccount(), gmUserVO.getPassword(), gmUserVO.getName());
        if (updatedGMUser != null) {
            return CommonVO.success();
        } else {
            return CommonVO.unsuccess("更新失败");
        }
    }

    @PostMapping("/delete_gm_user")
    @ResponseBody
    public CommonVO deleteGMUser(@RequestBody GetGMUserRequest getGMUserRequest) {
        GMUser gmUser = gmUserService.deleteGMUserByAccount(getGMUserRequest.getAccount());
        if (gmUser == null) {
            return CommonVO.unsuccess("用户不存在");
        }
        return CommonVO.success();
    }

    @PostMapping("/gold_field_list")
    @ResponseBody
    public CommonVO goldFieldList(@RequestBody SimpleListRequest simpleListRequest) {
        int pageNum = simpleListRequest.getCurrentPage();
        int pageSize = simpleListRequest.getPageSize();
        int from = (pageNum - 1) * pageSize;
        List<GoldField> goldFieldList = goldFieldService.getGoldFieldList(from, pageSize);
        int total = goldFieldService.countGoldFields();
        return CommonVO.success(new DataListPageVO(goldFieldList, total));
    }

    @PostMapping("/add_gold_field")
    @ResponseBody
    public CommonVO addGoldField(@RequestBody GoldField goldField) {
        GoldField addedGoldField = goldFieldService.addGoldField(
                goldField.getId(), goldField.getName(), goldField.getMinGold(), goldField.getMaxGold(),
                goldField.getCurrency(), goldField.getDifen());
        return CommonVO.success(addedGoldField);
    }

    @PostMapping("/delete_gold_field")
    @ResponseBody
    public CommonVO deleteGoldField(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GoldField goldField = goldFieldService.removeGoldField(locateEntityByIdRequest.getId());
        if (goldField == null) {
            return CommonVO.unsuccess("金币场不存在");
        }
        return CommonVO.success();
    }

    @PostMapping("/update_gold_field")
    @ResponseBody
    public CommonVO updateGoldField(@RequestBody GoldField goldField) {
        GoldField updatedGoldField = goldFieldService.modifyGoldField(
                goldField.getId(), goldField.getName(), goldField.getMinGold(), goldField.getMaxGold(),
                goldField.getCurrency(), goldField.getDifen());
        if (updatedGoldField == null) {
            return CommonVO.unsuccess("金币场不存在");
        }
        return CommonVO.success();
    }

    @PostMapping("/get_gold_field")
    @ResponseBody
    public CommonVO getGoldField(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GoldField goldField = goldFieldService.queryGoldField(locateEntityByIdRequest.getId());
        if (goldField == null) {
            return CommonVO.unsuccess("金币场不存在");
        }
        return CommonVO.success(goldField);
    }

    @PostMapping("/permission_list")
    @ResponseBody
    public CommonVO permissionList(@RequestBody SimpleListRequest simpleListRequest) {
        int pageNum = simpleListRequest.getCurrentPage();
        int pageSize = simpleListRequest.getPageSize();
        int from = (pageNum - 1) * pageSize;
        List<GMPermission> permissionList = gmRbacService.getPermissionList(from, pageSize);
        int total = gmRbacService.countPermissions();
        return CommonVO.success(new DataListPageVO(permissionList, total));
    }

    @PostMapping("/all_permission_list")
    @ResponseBody
    public CommonVO allPermissionList() {
        List<GMPermission> permissionList = gmRbacService.getAllPermissionList();
        return CommonVO.success(permissionList);
    }

    @PostMapping("/delete_permission")
    @ResponseBody
    public CommonVO deletePermission(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GMPermission permission = gmRbacService.removePermission(locateEntityByIdRequest.getId());
        if (permission == null) {
            return CommonVO.unsuccess("权限不存在");
        }
        msgSender.sendProcess();
        return CommonVO.success();
    }

    @PostMapping("/get_permission")
    @ResponseBody
    public CommonVO getPermission(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GMPermission permission = gmRbacService.getPermission(locateEntityByIdRequest.getId());
        if (permission == null) {
            return CommonVO.unsuccess("权限不存在");
        }
        return CommonVO.success(permission);
    }

    @PostMapping("/add_permission")
    @ResponseBody
    public CommonVO addPermission(@RequestBody GMPermission permission) {
        GMPermission addedPermission = gmRbacService.addPermission(permission.getId(), permission.getName());
        return CommonVO.success(addedPermission);
    }

    @PostMapping("/update_permission")
    @ResponseBody
    public CommonVO updatePermission(@RequestBody GMPermission permission) {
        GMPermission updatedPermission = gmRbacService.modifyPermission(permission.getId(), permission.getName());
        if (updatedPermission == null) {
            return CommonVO.unsuccess("权限不存在");
        }
        msgSender.sendProcess();
        return CommonVO.success();
    }

    @PostMapping("/role_list")
    @ResponseBody
    public CommonVO roleList(@RequestBody SimpleListRequest simpleListRequest) {
        int pageNum = simpleListRequest.getCurrentPage();
        int pageSize = simpleListRequest.getPageSize();
        int from = (pageNum - 1) * pageSize;
        List<GMRole> roleList = gmRbacService.getRoleList(from, pageSize);
        int total = gmRbacService.countRoles();
        return CommonVO.success(new DataListPageVO(GMRoleVO.convertList(roleList), total));
    }

    @PostMapping("/delete_role")
    @ResponseBody
    public CommonVO deleteRole(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GMRole role = gmRbacService.removeRole(locateEntityByIdRequest.getId());
        if (role == null) {
            return CommonVO.unsuccess("角色不存在");
        }
        msgSender.sendProcess();
        return CommonVO.success();
    }

    @PostMapping("/get_role")
    @ResponseBody
    public CommonVO getRole(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GMRole role = gmRbacService.getRole(locateEntityByIdRequest.getId());
        if (role == null) {
            return CommonVO.unsuccess("角色不存在");
        }
        return CommonVO.success(new GMRoleVO(role));
    }

    @PostMapping("/add_role")
    @ResponseBody
    public CommonVO addRole(@RequestBody GMRoleVO role) {
        GMRole addedRole = gmRbacService.addRole(role.getId(), role.getName(),
                role.getPermissionIdList() == null ? new HashSet<>() : role.getPermissionIdList().stream().collect(Collectors.toSet()));
        return CommonVO.success(addedRole);
    }

    @PostMapping("/update_role")
    @ResponseBody
    public CommonVO updateRole(@RequestBody GMRoleVO role) {
        GMRole updatedRole = gmRbacService.modifyRole(role.getId(), role.getName(), role.getPermissionIdList());
        if (updatedRole == null) {
            return CommonVO.unsuccess("角色不存在");
        }
        msgSender.sendProcess();
        return CommonVO.success();
    }

    @PostMapping("/user_permissions")
    @ResponseBody
    public CommonVO userPermissions(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        String account = locateEntityByIdRequest.getId();

        // 查询用户直接分配的权限
        Set<String> permissions = gmRbacService.getUserPermissions(account);

        // 查询所有权限
        List<GMPermission> allPermissions = gmRbacService.getAllPermissionList();

        // 查询用户通过角色获得的权限
        Set<String> rolePermissions = gmRbacService.getUserRolePermissions(account);

        // 拼成一个大 VO 作为 data 返回
        UserPermissionsVO userPermissionsVO = new UserPermissionsVO();
        userPermissionsVO.setPermissions(permissions);
        userPermissionsVO.setAllPermissions(allPermissions);
        userPermissionsVO.setRolePermissions(rolePermissions);

        return CommonVO.success(userPermissionsVO);
    }

    @PostMapping("/save_user_permissions")
    @ResponseBody
    public CommonVO saveUserPermissions(@RequestBody SaveUserPermissionsRequest request) {
        String account = request.getAccount();
        List<String> permissions = request.getPermissions();
        gmRbacService.saveUserPermissions(account, permissions);
        return CommonVO.success();
    }

    @PostMapping("/user_roles")
    @ResponseBody
    public CommonVO userRoles(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        String account = locateEntityByIdRequest.getId();

        // 查询用户分配的角色
        Set<String> roles = gmRbacService.getUserRoles(account);

        // 查询所有角色
        List<GMRole> allRoles = gmRbacService.getAllRoleList();

        // 拼成一个大 VO 作为 data 返回
        UserRolesVO userRolesVO = new UserRolesVO();
        userRolesVO.setRoles(roles);
        userRolesVO.setAllRoles(allRoles);

        return CommonVO.success(userRolesVO);
    }

    @PostMapping("/save_user_roles")
    @ResponseBody
    public CommonVO saveUserRoles(@RequestBody SaveUserRolesRequest request) {
        String account = request.getAccount();
        List<String> roles = request.getRoles();
        gmRbacService.saveUserRoles(account, roles);
        return CommonVO.success();
    }

    @PostMapping("/api_list")
    @ResponseBody
    public CommonVO apiList(@RequestBody GetApiListRequest getApiListRequest) {
        int page = getApiListRequest.getPage();
        int pageSize = getApiListRequest.getPageSize();
        String uri = getApiListRequest.getUri();
        int from = (page - 1) * pageSize;
        List<GMApi> apiList = gmApiService.getGMApiList(uri, from, pageSize);
        int total = gmApiService.countGMApis(uri);
        return CommonVO.success(new DataListPageVO(apiList, total));
    }

    @PostMapping("/delete_api")
    @ResponseBody
    public CommonVO deleteApi(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        gmApiService.deleteGMApi(locateEntityByIdRequest.getId());
        return CommonVO.success();
    }

    @PostMapping("/get_api")
    @ResponseBody
    public CommonVO getApi(@RequestBody LocateEntityByIdRequest locateEntityByIdRequest) {
        GMApi gmApi = gmApiService.getGMApi(locateEntityByIdRequest.getId());
        return CommonVO.success(gmApi);
    }

    @PostMapping("/add_api")
    @ResponseBody
    public CommonVO addApi(@RequestBody GMApi gmApi) {
        gmApiService.addGMApi(gmApi.getUri(), gmApi.getPermissionId());
        return CommonVO.success();
    }

    @PostMapping("/update_api")
    @ResponseBody
    public CommonVO updateApi(@RequestBody GMApi gmApi) {
        gmApiService.updateGMApi(gmApi.getUri(), gmApi.getPermissionId());
        return CommonVO.success();
    }

    @PostMapping("/count_online_hall_user")
    @ResponseBody
    public CommonVO countOnlineHallUser() {
        int count = loginService.countHallUserSession();
        return CommonVO.success(count);
    }

}
