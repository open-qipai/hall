package com.hall.web.ws;

import com.google.gson.Gson;
import com.hall.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

@Component
public class WsHandler extends TextWebSocketHandler {

    @Autowired
    private WsNotifier wsNotifier;

    @Autowired
    private LoginService loginService;

    private Gson gson = new Gson();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        CommonMO mo = new CommonMO();
        mo.setMsg("bindUser");
        sendMessage(session, gson.toJson(mo));
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        CommonMO mo = gson.fromJson(message.getPayload(), CommonMO.class);
        String msg = mo.getMsg();
        if ("bindUser".equals(msg)) {// 绑定玩家
            String userSessionId = (String) mo.getData();
            Long userId = loginService.getUserIdBySessionId(userSessionId);
            if (userId == null) {
                return;
            }
            wsNotifier.bindUser(userId, session);
        }
    }

    private void sendMessage(WebSocketSession session, String message) {
        try {
            session.sendMessage(new TextMessage(message));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
