package com.hall.web.ws;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WsNotifier {
    private Map<Long, WebSocketSession> userSessionMap = new ConcurrentHashMap<>();

    private Gson gson = new Gson();

    public void bindUser(long userId, WebSocketSession session) {
        userSessionMap.put(userId, session);
    }

    private void sendMessage(WebSocketSession session, String message) {
        if (session == null) {
            return;
        }
        synchronized (session) {
            try {
                session.sendMessage(new TextMessage(message));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void notifyMatchSuccess(long userId) {
        CommonMO mo = new CommonMO();
        mo.setMsg("matchSuccess");
        String payLoad = gson.toJson(mo);
        WebSocketSession session = userSessionMap.get(userId);
        sendMessage(session, payLoad);
    }

    public void notifyMatchFailure(long userId) {
        CommonMO mo = new CommonMO();
        mo.setMsg("matchFailure");
        String payLoad = gson.toJson(mo);
        WebSocketSession session = userSessionMap.get(userId);
        sendMessage(session, payLoad);
    }

    public void notifyNoGameServer(long userId) {
        CommonMO mo = new CommonMO();
        mo.setMsg("noGameServer");
        String payLoad = gson.toJson(mo);
        WebSocketSession session = userSessionMap.get(userId);
        sendMessage(session, payLoad);
    }

    public void notifyUserMoneyChanged(long userId) {
        CommonMO mo = new CommonMO();
        mo.setMsg("moneyChanged");
        String payLoad = gson.toJson(mo);
        WebSocketSession session = userSessionMap.get(userId);
        sendMessage(session, payLoad);
    }
}
