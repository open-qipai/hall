package com.hall.web;

import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.money.MoneyAccount;
import com.hall.entity.user.HallUser;
import com.hall.entity.user.HallUserSession;
import com.hall.msg.MsgSender;
import com.hall.service.HallGamePlayService;
import com.hall.service.LoginService;
import com.hall.service.MoneyService;
import com.hall.service.result.LoginResult;
import com.hall.web.request.OpenIDLoginRequest;
import com.hall.web.viewobject.CommonVO;
import com.hall.web.viewobject.UserGamePlayVO;
import com.hall.web.viewobject.UserInfoVO;
import dml.user.service.result.LoginByOpenIDResult;
import erp.ERP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hall/user")
public class UserController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private HallGamePlayService hallGamePlayService;

    @Autowired
    private MoneyService moneyService;

    @Autowired
    private MsgSender msgSender;

    @PostMapping("/openid_login")
    @ResponseBody
    public CommonVO openIDLogin(@RequestBody OpenIDLoginRequest request) {
        String openID = request.getId();
        String nickname = request.getNickname();
        LoginResult loginResult =
                ERP.retry(() -> loginService.login(openID, System.currentTimeMillis(), nickname), 256, 10).getReturn();
        LoginByOpenIDResult loginByOpenIDResult = loginResult.getLoginByOpenIDResult();
        HallUser hallUser = loginResult.getHallUser();
        msgSender.sendProcess();
        HallUserSession session = (HallUserSession) loginByOpenIDResult.getNewUserSession();
        return CommonVO.success("token", session.getId()).append("userId", session.getUserID().toString())
                .append("nickname", hallUser.getNickname())
                .append("serialNumber", hallUser.getSerialNumber());
    }

    @PostMapping("/game_play")
    @ResponseBody
    public CommonVO gamePlay(@RequestAttribute("userId") long userId) {
        HallGamePlay hallGamePlay = hallGamePlayService.getCurrentGamePlayForUser(userId);
        return CommonVO.success(new UserGamePlayVO(hallGamePlay));
    }

    @PostMapping("/user_info_for_game_play")
    @ResponseBody
    public CommonVO userInfoForGamePlay(@RequestAttribute("userId") long userId) {
        HallGamePlay hallGamePlay = hallGamePlayService.getCurrentGamePlayForUser(userId);
        List playerIdList = hallGamePlay.getPlayerIdList();
        List<HallUser> userList = loginService.findHallUserList(playerIdList);
        List<MoneyAccount> goldAccountList = moneyService.findMoneyAccountForUsers(playerIdList, "gold");
        return CommonVO.success(UserInfoVO.fromUserListAndGoldAccountList(userList, goldAccountList));
    }

}
