package com.hall.web.viewobject;

import com.hall.entity.money.MoneyAccount;
import com.hall.entity.user.HallUser;

import java.util.ArrayList;
import java.util.List;

public class UserInfoVO {
    private String userId;
    private String nickname;
    private String gold;

    public UserInfoVO() {
    }

    public UserInfoVO(HallUser hallUser, MoneyAccount moneyAccount) {
        this.userId = hallUser.getId() + "";
        this.nickname = hallUser.getNickname();
        this.gold = moneyAccount.getBalance();
    }

    public static List<UserInfoVO> fromUserListAndGoldAccountList(List<HallUser> userList, List<MoneyAccount> goldAccountList) {
        List<UserInfoVO> userInfoVOList = new ArrayList<>();
        int i = 0;
        for (HallUser hallUser : userList) {
            MoneyAccount moneyAccount = goldAccountList.get(i);
            UserInfoVO userInfoVO = new UserInfoVO(hallUser, moneyAccount);
            userInfoVOList.add(userInfoVO);
            i++;
        }
        return userInfoVOList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGold() {
        return gold;
    }

    public void setGold(String gold) {
        this.gold = gold;
    }
}
