package com.hall.web.viewobject;

import com.hall.entity.gameplay.HallGamePlay;

public class UserGamePlayVO {

    private String gameId;
    private String gameServerOuterHost;
    private String gameServerInnerHost;
    private String gameServerGameId;

    public UserGamePlayVO() {
    }

    public UserGamePlayVO(HallGamePlay hallGamePlay) {
        this.gameId = hallGamePlay.getGameId();
        this.gameServerOuterHost = hallGamePlay.getGameServerOuterHost();
        this.gameServerInnerHost = hallGamePlay.getGameServerInnerHost();
        this.gameServerGameId = hallGamePlay.getGameServerGameId();
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameServerOuterHost() {
        return gameServerOuterHost;
    }

    public void setGameServerOuterHost(String gameServerOuterHost) {
        this.gameServerOuterHost = gameServerOuterHost;
    }

    public String getGameServerInnerHost() {
        return gameServerInnerHost;
    }

    public void setGameServerInnerHost(String gameServerInnerHost) {
        this.gameServerInnerHost = gameServerInnerHost;
    }

    public String getGameServerGameId() {
        return gameServerGameId;
    }

    public void setGameServerGameId(String gameServerGameId) {
        this.gameServerGameId = gameServerGameId;
    }
}
