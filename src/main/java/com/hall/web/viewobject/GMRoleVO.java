package com.hall.web.viewobject;

import com.hall.entity.gm.GMRole;

import java.util.List;
import java.util.stream.Collectors;

public class GMRoleVO {
    private String id;
    private String name;
    private List<String> permissionIdList;

    public GMRoleVO() {

    }

    public GMRoleVO(GMRole gmRole) {
        this.id = gmRole.getId();
        this.name = gmRole.getName();
        this.permissionIdList = gmRole.getPermissions() != null ? gmRole.getPermissions().stream().collect(Collectors.toList()) : null;
    }

    public static List<GMRoleVO> convertList(List<GMRole> gmRoleList) {
        return gmRoleList.stream().map(GMRoleVO::new).collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPermissionIdList() {
        return permissionIdList;
    }

    public void setPermissionIdList(List<String> permissionIdList) {
        this.permissionIdList = permissionIdList;
    }
}
