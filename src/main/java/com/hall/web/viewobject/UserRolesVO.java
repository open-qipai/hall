package com.hall.web.viewobject;

import com.hall.entity.gm.GMRole;

import java.util.List;
import java.util.Set;

public class UserRolesVO {
    private Set<String> roles;
    private List<GMRole> allRoles;

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public List<GMRole> getAllRoles() {
        return allRoles;
    }

    public void setAllRoles(List<GMRole> allRoles) {
        this.allRoles = allRoles;
    }
}
