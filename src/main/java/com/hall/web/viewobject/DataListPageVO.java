package com.hall.web.viewobject;

import java.util.List;

public class DataListPageVO {
    private List list;
    private int total;

    public DataListPageVO() {
    }

    public DataListPageVO(List list, int total) {
        this.list = list;
        this.total = total;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
