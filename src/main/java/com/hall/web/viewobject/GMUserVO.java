package com.hall.web.viewobject;

import com.hall.entity.gm.GMUser;

import java.util.ArrayList;
import java.util.List;

public class GMUserVO {

    private String account;
    private String password;
    private String name;
    private String createTime;

    public GMUserVO() {
    }

    public GMUserVO(GMUser gmUser) {
        this.account = gmUser.getAccount();
        this.password = gmUser.getPassword();
        this.name = gmUser.getName();
        this.createTime = String.valueOf(gmUser.getCreateTime());
    }

    public static List<GMUserVO> toVOList(List<GMUser> gmUserList) {
        List<GMUserVO> gmUserVOList = new ArrayList<>();
        for (GMUser gmUser : gmUserList) {
            gmUserVOList.add(new GMUserVO(gmUser));
        }
        return gmUserVOList;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
