package com.hall.web.viewobject;

import com.hall.entity.gm.GMPermission;

import java.util.List;
import java.util.Set;

public class UserPermissionsVO {
    private Set<String> permissions;
    private List<GMPermission> allPermissions;
    private Set<String> rolePermissions;

    // Getters and Setters
    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    public List<GMPermission> getAllPermissions() {
        return allPermissions;
    }

    public void setAllPermissions(List<GMPermission> allPermissions) {
        this.allPermissions = allPermissions;
    }

    public Set<String> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(Set<String> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}
