package com.hall.web;

import com.hall.entity.money.MoneyAccount;
import com.hall.entity.user.HallUser;
import com.hall.msg.MsgSender;
import com.hall.service.LoginService;
import com.hall.service.MoneyService;
import com.hall.service.result.BatchRegisterUsersResult;
import com.hall.web.request.BatchCreateUserRequest;
import com.hall.web.request.BotRB;
import com.hall.web.request.LocateEntityByIdRequest;
import com.hall.web.viewobject.CommonVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/for_bots")
public class BotsController {

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private LoginService loginService;

    @Autowired
    private MoneyService moneyService;

    @PostMapping("/batch_create_user")
    public CommonVO batchCreateUser(@RequestBody BatchCreateUserRequest request) {
        List<Map> inputUserList = new ArrayList<>();
        for (BotRB botRB : request.getBotList()) {
            Map<String, Object> inputUser = new HashMap<>();
            inputUser.put("openId", botRB.getOpenId());
            inputUser.put("nickname", botRB.getNickname());
            inputUserList.add(inputUser);
        }
        BatchRegisterUsersResult batchRegisterUsersResult = loginService.batchRegisterUsers(inputUserList);
        moneyService.batchSendGold(batchRegisterUsersResult.getHallUserIdList(), request.getInitGold(), "初始金币");
        msgSender.sendProcess();
        Map<String, HallUser> openIdToHallUserMap = batchRegisterUsersResult.getOpenIdToHallUserMap();
        List<Map> userList = new ArrayList<>();
        for (Map.Entry<String, HallUser> entry : openIdToHallUserMap.entrySet()) {
            HallUser hallUser = entry.getValue();
            Map<String, Object> user = new HashMap<>();
            user.put("openId", entry.getKey());
            user.put("userId", hallUser.getId().toString());
            userList.add(user);
        }
        return CommonVO.success(userList);
    }

    @PostMapping("/deregister_user")
    public CommonVO deregisterUser(@RequestBody LocateEntityByIdRequest request) {
        String userIdStr = request.getId();
        long userId = Long.parseLong(userIdStr);
        loginService.deregisterUser(userId);
        return CommonVO.success();
    }

    @PostMapping("/get_user_info")
    public CommonVO getUserInfo(@RequestBody LocateEntityByIdRequest request) {
        String userIdStr = request.getId();
        long userId = Long.parseLong(userIdStr);
        HallUser hallUser = loginService.getUserById(userId);
        MoneyAccount goldAccount = moneyService.getGoldAccount(userId);
        Map<String, Object> user = new HashMap<>();
        user.put("userId", hallUser.getId().toString());
        user.put("gold", goldAccount.getBalance());
        return CommonVO.success(user);
    }

}
