package com.hall.web;

import com.google.gson.Gson;
import com.hall.entity.gameplay.GameMode;
import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.gameplay.WzmjSettle;
import com.hall.service.AllocationService;
import com.hall.service.HallGamePlayService;
import com.hall.service.SettleService;
import com.hall.web.request.GameServerRegisterRequest;
import com.hall.web.request.GameServerUpdateStatusRequest;
import com.hall.web.request.SettleRequest;
import com.hall.web.viewobject.CommonVO;
import com.hall.web.ws.WsNotifier;
import erp.ERP;
import erp.repository.TakeEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/gs")
public class GameServerController {

    @Autowired
    private AllocationService allocationService;

    @Autowired
    private HallGamePlayService hallGamePlayService;

    @Autowired
    private SettleService settleService;

    @Autowired
    private WsNotifier wsNotifier;

    private Gson gson = new Gson();

    /**
     * 注册游戏服务器
     */
    @PostMapping("/register")
    @ResponseBody
    public CommonVO register(@RequestBody GameServerRegisterRequest request) {
        ERP.retry(() -> allocationService.registerGameServer(request.getGameServerId(), request.getMaxPlayer(), request.getGameId(),
                        request.getOuterHost(), request.getInnerHost(),
                        System.currentTimeMillis(), 10000L),
                3, 100);
        return CommonVO.success();
    }

    /**
     * 更新游戏服务器状态
     */
    @PostMapping("/update_status")
    @ResponseBody
    public CommonVO updateStatus(@RequestBody GameServerUpdateStatusRequest request) {
        ERP.retry(() -> allocationService.updateGameServerStatus(request.getGameServerId(), request.getCurrentPlayers(), System.currentTimeMillis()),
                3, 100);
        return CommonVO.success();
    }

    /**
     * 结算
     */
    @PostMapping("/settle")
    @ResponseBody
    public CommonVO settle(@RequestBody SettleRequest request) {
        long gamePlayId = Long.parseLong(request.getGamePlayId());
        String settleJson = request.getSettleJson();
        HallGamePlay hallGamePlay = null;
        try {
            hallGamePlay = hallGamePlayService.endGame(gamePlayId);
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will fix battle bug later");
            return CommonVO.success();
        }
        GameMode gameMode = hallGamePlay.getGameMode();
        if (gameMode.equals(GameMode.jinbichang_wzmj)) {
            WzmjSettle wzmjSettle = gson.fromJson(settleJson, WzmjSettle.class);
            long huPlayerId = Long.parseLong(wzmjSettle.getHuPlayerId());
            settleService.settleJinbichangWzmj(gamePlayId, huPlayerId);
            wsNotifier.notifyUserMoneyChanged(huPlayerId);
        }
        return CommonVO.success();
    }

    /**
     * 流局
     */
    @PostMapping("/liuju")
    @ResponseBody
    public CommonVO liuju(@RequestBody SettleRequest request) {
        long gamePlayId = Long.parseLong(request.getGamePlayId());
        try {
            hallGamePlayService.endGame(gamePlayId);
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will fix battle bug later");
        }
        return CommonVO.success();
    }

}
