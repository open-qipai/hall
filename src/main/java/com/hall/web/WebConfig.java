package com.hall.web;

import com.hall.web.interceptor.GMUserInterceptor;
import com.hall.web.interceptor.HallUserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private GMUserInterceptor gmUserInterceptor;

    @Autowired
    private HallUserInterceptor hallUserInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:7456", "http://localhost:5173", "http://jsj004.vicp.net:29002/")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .allowedHeaders("*")
                .allowCredentials(true);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(gmUserInterceptor)
                .addPathPatterns("/gm/**") // 拦截 /gm/** 路径下的所有请求
                .excludePathPatterns("/gm/login"); // 排除登录请求
        registry.addInterceptor(hallUserInterceptor)
                .addPathPatterns("/hall/**") // 拦截 /hall/** 路径下的所有请求
                .excludePathPatterns("/hall/user/openid_login"); // 排除登录请求
    }
}