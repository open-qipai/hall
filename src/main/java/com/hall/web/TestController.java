package com.hall.web;

import com.hall.service.TestService;
import com.hall.web.request.TestLoginRequest;
import com.hall.web.viewobject.CommonVO;
import dml.user.entity.UserCurrentSession;
import erp.ERP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService testService;

    /**
     * 创建1000个session
     */
    @PostMapping("/createSession")
    @ResponseBody
    public CommonVO createSession() {
        for (int i = 0; i < 1000; i++) {
            testService.createSession();
        }
        return CommonVO.success();
    }

    /**
     * 查询session
     */
    @PostMapping("/querySession")
    @ResponseBody
    public CommonVO querySession() {
        return CommonVO.success(testService.querySession());
    }

    /**
     * 注册
     */
    @PostMapping("/register")
    @ResponseBody
    public CommonVO register() {
        UserCurrentSession userCurrentSession =
                ERP.retry(() -> testService.register(), 16, 100).getReturn();
        String userId = String.valueOf(userCurrentSession.getUserID());
        return CommonVO.success(userId);
    }

    /**
     * 注册1000个用户
     */
    @PostMapping("/register1k")
    @ResponseBody
    public CommonVO register1k() {
        for (int i = 0; i < 1000; i++) {
            testService.register();
        }
        return CommonVO.success();
    }

    /**
     * 登录
     */
    @PostMapping("/login")
    @ResponseBody
    public CommonVO login(@RequestBody TestLoginRequest request) {
        return CommonVO.success(
                ERP.retry(() -> testService.login(Long.parseLong(request.getUserId())), 16, 100).getReturn());
    }

    /**
     * 删除session
     */
    @PostMapping("/removeSession")
    @ResponseBody
    public CommonVO removeSession() {
        testService.removeSession();
        return CommonVO.success();
    }
}
