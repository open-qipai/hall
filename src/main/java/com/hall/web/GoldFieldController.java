package com.hall.web;

import com.hall.service.GoldFieldService;
import com.hall.service.result.JoinGoldFieldResult;
import com.hall.web.request.GoldFieldJoinRequest;
import com.hall.web.viewobject.CommonVO;
import dml.gamematch.service.exception.PlayerAlreadyInMatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hall/gold_field")
public class GoldFieldController {

    @Autowired
    private GoldFieldService goldFieldService;

    @PostMapping("/join")
    @ResponseBody
    public CommonVO join(@RequestAttribute("userId") long userId,
                         @RequestBody GoldFieldJoinRequest goldFieldJoinRequest) {
        JoinGoldFieldResult joinGoldFieldResult = null;
        try {
            joinGoldFieldResult = goldFieldService.joinGoldField(userId, goldFieldJoinRequest.getGoldFieldId(),
                    System.currentTimeMillis());
        } catch (PlayerAlreadyInMatchException e) {
            return CommonVO.unsuccess(e.getMessage());
        }
        if (joinGoldFieldResult.isSuccess()) {
            return CommonVO.success();
        } else if (joinGoldFieldResult.isBalanceNotInRange()) {
            return CommonVO.unsuccess("balance_not_in_range");
        } else {
            return CommonVO.unsuccess("join_failed");
        }
    }

    @PostMapping("/cancel")
    @ResponseBody
    public CommonVO cancel(@RequestAttribute("userId") long userId,
                           @RequestBody GoldFieldJoinRequest goldFieldJoinRequest) {
        goldFieldService.cancelGoldFieldMatch(userId, goldFieldJoinRequest.getGoldFieldId());
        return CommonVO.success();
    }

}
