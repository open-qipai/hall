package com.hall.web.request;

import java.util.List;

public class SaveUserPermissionsRequest {
    private String account;
    private List<String> permissions;

    // Getters and Setters
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}