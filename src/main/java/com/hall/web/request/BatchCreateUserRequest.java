package com.hall.web.request;

import java.util.List;

public class BatchCreateUserRequest {

    private List<BotRB> botList;

    private int initGold;

    public List<BotRB> getBotList() {
        return botList;
    }

    public void setBotList(List<BotRB> botList) {
        this.botList = botList;
    }

    public int getInitGold() {
        return initGold;
    }

    public void setInitGold(int initGold) {
        this.initGold = initGold;
    }
}
