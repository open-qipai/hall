package com.hall.web.request;

public class GameServerRegisterRequest {
    private String gameId;
    private String gameServerId;
    private String outerHost;
    private String innerHost;
    private int maxPlayer;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameServerId() {
        return gameServerId;
    }

    public void setGameServerId(String gameServerId) {
        this.gameServerId = gameServerId;
    }

    public String getOuterHost() {
        return outerHost;
    }

    public void setOuterHost(String outerHost) {
        this.outerHost = outerHost;
    }

    public String getInnerHost() {
        return innerHost;
    }

    public void setInnerHost(String innerHost) {
        this.innerHost = innerHost;
    }

    public int getMaxPlayer() {
        return maxPlayer;
    }

    public void setMaxPlayer(int maxPlayer) {
        this.maxPlayer = maxPlayer;
    }
}
