package com.hall.web.request;

import java.util.List;

public class SaveUserRolesRequest {
    private String account;
    private List<String> roles;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
