package com.hall.web.request;

public class GoldFieldJoinRequest {
    private String goldFieldId;

    public String getGoldFieldId() {
        return goldFieldId;
    }

    public void setGoldFieldId(String goldFieldId) {
        this.goldFieldId = goldFieldId;
    }
}
