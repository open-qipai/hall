package com.hall.web.request;

public class LocateEntityByIdRequest {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
