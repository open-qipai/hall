package com.hall.web.request;

public class SettleRequest {
    private String gamePlayId;
    private String settleJson;

    public String getGamePlayId() {
        return gamePlayId;
    }

    public void setGamePlayId(String gamePlayId) {
        this.gamePlayId = gamePlayId;
    }

    public String getSettleJson() {
        return settleJson;
    }

    public void setSettleJson(String settleJson) {
        this.settleJson = settleJson;
    }
}
