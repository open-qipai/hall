package com.hall.web.interceptor;

import com.hall.entity.gm.GMApi;
import com.hall.service.gm.GMApiService;
import com.hall.service.gm.GMRbacService;
import com.hall.service.gm.GMUserService;
import erp.repository.TakeEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class GMUserInterceptor implements HandlerInterceptor {

    @Autowired
    private GMRbacService gmRbacService;
    @Autowired
    private GMUserService gmUserService;
    @Autowired
    private GMApiService gmApiService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //对OPTIONS方法的跨域预检请求直接放过
        if ("OPTIONS".equals(request.getMethod())) {
            return true;
        }
        //从authorization获得sessionId
        String sessionId = request.getHeader("Authorization");
        String gmAccount = gmUserService.getGmAccountBySessionId(sessionId);
        if (gmAccount == null) {
            response.setStatus(401);
            return false;
        }
        String reqUri = request.getRequestURI();
        GMApi gmApi = gmApiService.getGMApi(reqUri);
        if (gmApi != null) {
            String permissionId = gmApi.getPermissionId();
            if (permissionId != null) {
                if (!gmRbacService.hasPermission(gmAccount, permissionId)) {
                    response.setStatus(403);
                    return false;
                }
            }
        }

        try {
            gmUserService.keepGmUserSessionAlive(sessionId, System.currentTimeMillis());
        } catch (TakeEntityException e) {
        }
        //把account放到request中，后续的Controller可以直接使用
        request.setAttribute("account", gmAccount);
        return true;
    }
}