package com.hall.web.interceptor;

import com.hall.service.LoginService;
import erp.repository.TakeEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class HallUserInterceptor implements HandlerInterceptor {
    @Autowired
    private LoginService loginService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //对OPTIONS方法的跨域预检请求直接放过
        if ("OPTIONS".equals(request.getMethod())) {
            return true;
        }
        //从authorization获得sessionId
        String sessionId = request.getHeader("Authorization");
        Long userId = loginService.getUserIdBySessionId(sessionId);
        if (userId == null) {
            response.setStatus(401);
            return false;
        }

        try {
            loginService.keepHallUserSessionAlive(sessionId, System.currentTimeMillis());
        } catch (TakeEntityException e) {
        }

        //把userId放到request中，后续的Controller可以直接使用
        request.setAttribute("userId", userId);
        return true;
    }
}
