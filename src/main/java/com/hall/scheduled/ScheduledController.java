package com.hall.scheduled;

import com.hall.client.GameServerClient;
import com.hall.entity.allocation.AllocationGameServer;
import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.goldfield.GoldField;
import com.hall.msg.MsgSender;
import com.hall.service.*;
import com.hall.web.ws.WsNotifier;
import erp.repository.TakeEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduledController {

    @Autowired
    private ClearSessionService clearSessionService;

    @Autowired
    private GoldFieldService goldFieldService;

    @Autowired
    private AllocationService allocationService;

    @Autowired
    private HallGamePlayService hallGamePlayService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private MoneyService moneyService;

    @Autowired
    private WsNotifier wsNotifier;

    @Autowired
    private GameServerClient gameServerClient;

    @Autowired
    private MsgSender msgSender;

    @Scheduled(fixedRate = 300000)
    public void clearSession() {
        int sessionBatchSize = 1000;
        long maxSegmentExecutionTime = 60 * 1000;
        long maxTimeToTaskReady = 60 * 1000;
        long sessionKeepAliveInterval = 60 * 60 * 1000;
        boolean createTaskSuccess = clearSessionService.createUserSessionCleanupTask(System.currentTimeMillis());
        if (createTaskSuccess) {
            clearSessionService.addAllSessionIdToUserSessionCleanupTask(sessionBatchSize);
        }
        while (true) {
            String segmentId = clearSessionService.takeUserSessionCleanupTaskSegmentToExecute(System.currentTimeMillis(),
                    maxSegmentExecutionTime, maxTimeToTaskReady);
            if (segmentId == null) {
                break;
            }
            clearSessionService.executeUserSessionCleanupTask(segmentId, System.currentTimeMillis(), sessionKeepAliveInterval);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 温州麻将初级金币场匹配定时任务
     */
    @Scheduled(fixedRate = 1000)
    public void matchLowLevelGoldFieldForWenzhouMahjong() {
        try {
            matchGoldField("chujichang");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将中级金币场匹配定时任务
     */
    @Scheduled(fixedRate = 1000)
    public void matchMiddleLevelGoldFieldForWenzhouMahjong() {
        try {
            matchGoldField("zhongjichang");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将高级金币场匹配定时任务
     */
    @Scheduled(fixedRate = 1000)
    public void matchHighLevelGoldFieldForWenzhouMahjong() {
        try {
            matchGoldField("gaojichang");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    private void matchGoldField(String goldFieldId) {
        long currentTime = System.currentTimeMillis();
        List<String> notMatchedPlayers = goldFieldService.matchGoldField(goldFieldId, currentTime);
        GoldField goldField = goldFieldService.queryGoldField(goldFieldId);
        msgSender.sendNotMatchedPlayers(goldField, notMatchedPlayers);
    }

    /**
     * 温州麻将初级金币场处理匹配结果
     */
    @Scheduled(fixedRate = 1000)
    public void processLowLevelGoldFieldMatchResultForWenzhouMahjong() {
        try {
            processGoldFieldMatchResult("chujichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将中级金币场处理匹配结果
     */
    @Scheduled(fixedRate = 1000)
    public void processMiddleLevelGoldFieldMatchResultForWenzhouMahjong() {
        try {
            processGoldFieldMatchResult("zhongjichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将高级金币场处理匹配结果
     */
    @Scheduled(fixedRate = 1000)
    public void processHighLevelGoldFieldMatchResultForWenzhouMahjong() {
        try {
            processGoldFieldMatchResult("gaojichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将初级金币场处理匹配失败
     */
    @Scheduled(fixedRate = 1000)
    public void processLowLevelGoldFieldMatchFailureForWenzhouMahjong() {
        try {
            processGoldFieldMatchFailure("chujichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将中级金币场处理匹配失败
     */
    @Scheduled(fixedRate = 1000)
    public void processMiddleLevelGoldFieldMatchFailureForWenzhouMahjong() {
        try {
            processGoldFieldMatchFailure("zhongjichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    /**
     * 温州麻将高级金币场处理匹配失败
     */
    @Scheduled(fixedRate = 1000)
    public void processHighLevelGoldFieldMatchFailureForWenzhouMahjong() {
        try {
            processGoldFieldMatchFailure("gaojichang", "wenzhou_mahjong");
        } catch (TakeEntityException e) {
            System.out.println("OK to catch TakeEntityException, will retry");
        }
    }

    private void processGoldFieldMatchFailure(String goldFieldId, String gameId) {
        while (true) {
            long currentTime = System.currentTimeMillis();
            String failurePlayerId = goldFieldService.acquireMatchFailurePlayerId(goldFieldId, currentTime);
            if (failurePlayerId == null) {
                return;
            }
            wsNotifier.notifyMatchFailure(Long.parseLong(failurePlayerId));
        }
    }

    private void processGoldFieldMatchResult(String goldFieldId, String gameId) {
        while (true) {
            long currentTime = System.currentTimeMillis();
            List<String> matchedPlayerIdStrList = goldFieldService.acquireMatchedPlayers(goldFieldId, currentTime);
            if (matchedPlayerIdStrList == null) {
                return;
            }
            //allocation分配战斗服
            AllocationGameServer allocatedGameServer = allocationService.allocateGameServer(gameId, matchedPlayerIdStrList.size(), currentTime);
            if (allocatedGameServer == null) {
                //推送玩家没有合适的服务器
                for (String playerId : matchedPlayerIdStrList) {
                    wsNotifier.notifyNoGameServer(Long.parseLong(playerId));
                }
                continue;
            }
            List<Long> matchedPlayerIdList = new ArrayList<>();
            for (String playerId : matchedPlayerIdStrList) {
                matchedPlayerIdList.add(Long.parseLong(playerId));
            }
            //调用战斗服接口，开始游戏。目前没有“局”，会直接开始一盘
            String gameServerGameId = gameServerClient.startGame(allocatedGameServer.getInnerHost(), matchedPlayerIdStrList, true);
            //记录玩家游玩
            HallGamePlay hallGamePlay = hallGamePlayService.playJinbichangGame(matchedPlayerIdList, gameId, gameServerGameId,
                    allocatedGameServer.getInnerHost(), allocatedGameServer.getOuterHost(), goldFieldId);
            //调用战斗服接口，给战斗服game绑定大厅gamePlayId
            gameServerClient.bindGamePlayId(allocatedGameServer.getInnerHost(), gameServerGameId, hallGamePlay.getId().toString());
            //扣除金币
            goldFieldService.deductGold(matchedPlayerIdList, goldFieldId);
            //推送玩家匹配成功,客户端接到这个消息后，查询User的当前GamePlay
            for (String playerId : matchedPlayerIdStrList) {
                wsNotifier.notifyMatchSuccess(Long.parseLong(playerId));
            }
        }
    }

    /**
     * allocation定时来选出 温州麻将 最佳服务器
     */
    @Scheduled(fixedRate = 1000)
    public void selectBestGameServerForWenzhouMahjong() {
        String gameId = "wenzhou_mahjong";
        allocationService.selectBestGameServer(gameId,
                System.currentTimeMillis(), 5 * 1000L, 60 * 1000L);
    }


}
