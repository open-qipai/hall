package com.hall.scheduled;

import com.hall.msg.MsgSender;
import com.hall.service.gm.GMRbacService;
import com.hall.service.gm.GMUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GMScheduledController {

    @Autowired
    private GMUserService gmUserService;

    @Autowired
    private GMRbacService gmRbacService;

    @Autowired
    private MsgSender msgSender;

    @Scheduled(fixedRate = 300000)
    public void clearGmUserSession() {
        while (true) {
            boolean goon = gmUserService.executeGmUserSessionCleanupTask("ClearGmUserSessionTask", System.currentTimeMillis(),
                    1000, 60 * 1000, 60 * 1000, 60 * 60 * 1000);
            if (!goon) {
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Scheduled(fixedRate = 1000)
    public void clearRolePermissions() {
        List<String> taskNameList = gmRbacService.getAllRolePermissionCleanupTaskName();
        for (String taskName : taskNameList) {
            while (true) {
                boolean goon = gmRbacService.executeRolePermissionCleanupTask(taskName, System.currentTimeMillis());
                msgSender.sendProcess();
                if (!goon) {
                    break;
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Scheduled(fixedRate = 1000)
    public void clearUserRoles() {
        List<String> taskNameList = gmRbacService.getAllUserRoleCleanupTaskName();
        for (String taskName : taskNameList) {
            while (true) {
                boolean goon = gmRbacService.executeUserRoleCleanupTask(taskName, System.currentTimeMillis());
                if (!goon) {
                    break;
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Scheduled(fixedRate = 1000)
    public void clearUserPermissions() {
        List<String> taskNameList = gmRbacService.getAllUserPermissionCleanupTaskName();
        for (String taskName : taskNameList) {
            while (true) {
                boolean goon = gmRbacService.executeUserPermissionCleanupTask(taskName, System.currentTimeMillis());
                if (!goon) {
                    break;
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
