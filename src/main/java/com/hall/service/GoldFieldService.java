package com.hall.service;

import com.hall.entity.goldfield.GoldField;
import com.hall.entity.money.MoneyBillItem;
import com.hall.repository.*;
import com.hall.service.result.JoinGoldFieldResult;
import dml.gamecurrency.repository.GameCurrencyAccountBillItemRepository;
import dml.gamecurrency.repository.GameCurrencyAccountIdGeneratorRepository;
import dml.gamecurrency.repository.GameCurrencyAccountRepository;
import dml.gamecurrency.repository.GameUserCurrencyAccountsRepository;
import dml.gamecurrency.service.GameCurrencyAccountingService;
import dml.gamecurrency.service.repositoryset.GameCurrencyAccountingServiceRepositorySet;
import dml.gamematch.entity.randommatch.RandomMatchPlayer;
import dml.gamematch.repository.*;
import dml.gamematch.service.RandomMatchService;
import dml.gamematch.service.exception.PlayerAlreadyInMatchException;
import dml.gamematch.service.repositoryset.RandomMatchServiceRepositorySet;
import dml.gamematch.service.result.RandomMatchResult;
import erp.annotation.Process;
import erp.redis.pipeline.RedisPipeline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 金币场服务，针对 GoldField 的增删改查。列表查询为分页查询。
 * Repository使用对应接口的mongodb实现。
 * 构造器注入MongoTemplate。
 */
@Component
public class GoldFieldService implements
        GameCurrencyAccountingServiceRepositorySet,
        RandomMatchServiceRepositorySet {

    @Autowired
    private GoldFieldRepository goldFieldRepository;
    @Autowired
    private MoneyAccountRepository moneyAccountRepository;
    @Autowired
    private GameCurrencyAccountIdGeneratorRepository gameCurrencyAccountIdGeneratorRepository;
    @Autowired
    private GameUserCurrencyAccountsRepository gameUserCurrencyAccountsRepository;
    @Autowired
    private MoneyBillItemRepository moneyBillItemRepository;
    @Autowired
    private GameCurrencyAccountBillItemIdGeneratorRepository gameCurrencyAccountBillItemIdGeneratorRepository;
    @Autowired
    private MatchStateRepository goldFieldMatchStateRepository;
    @Autowired
    private GoldFieldRandomMatchPlayerRepository goldFieldRandomMatchPlayerRepository;
    @Autowired
    private MatchResultAcquisitionTaskRepository goldFieldMatchResultAcquisitionTaskRepository;
    @Autowired
    private MatchResultAcquisitionTaskSegmentRepository goldFieldMatchResultAcquisitionTaskSegmentRepository;
    @Autowired
    private MatchResultAcquisitionTaskSegmentIDGeneratorRepository goldFieldMatchResultAcquisitionTaskSegmentIDGeneratorRepository;
    @Autowired
    private PlayerMatchFailureAcquisitionTaskRepository goldFieldPlayerMatchFailureAcquisitionTaskRepository;
    @Autowired
    private PlayerMatchFailureAcquisitionTaskSegmentRepository goldFieldPlayerMatchFailureAcquisitionTaskSegmentRepository;
    @Autowired
    private PlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository goldFieldPlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository;
    @Autowired
    private PlayerCurrentMatchRepository playerCurrentMatchRepository;


    /**
     * 添加金币场
     */
    @Process
    public GoldField addGoldField(String id, String name, int minGold, int maxGold, String currency, int difen) {
        GoldField goldField = new GoldField();
        goldField.setId(id);
        goldField.setName(name);
        goldField.setMinGold(minGold);
        goldField.setMaxGold(maxGold);
        goldField.setCurrency(currency);
        goldField.setDifen(difen);
        goldFieldRepository.put(goldField);
        return goldField;
    }

    /**
     * 删除金币场
     */
    @Process
    public GoldField removeGoldField(String id) {
        return goldFieldRepository.remove(id);
    }

    /**
     * 修改金币场
     */
    @Process
    public GoldField modifyGoldField(String id, String name, int minGold, int maxGold, String currency, int difen) {
        GoldField goldField = goldFieldRepository.take(id);
        if (goldField == null) {
            return null;
        }
        goldField.setName(name);
        goldField.setMinGold(minGold);
        goldField.setMaxGold(maxGold);
        goldField.setCurrency(currency);
        goldField.setDifen(difen);
        return goldField;
    }

    /**
     * 查询金币场
     */
    public GoldField queryGoldField(String id) {
        return goldFieldRepository.find(id);
    }

    /**
     * 分页查询金币场
     */
    public List<GoldField> getGoldFieldList(int from, int size) {
        return goldFieldRepository.list(from, size);
    }

    /**
     * 查询金币场总数
     */
    public int countGoldFields() {
        return goldFieldRepository.countAll();
    }

    @Process
    @RedisPipeline
    public JoinGoldFieldResult joinGoldField(long userId, String goldFieldId, long currentTime) throws PlayerAlreadyInMatchException {
        JoinGoldFieldResult result = new JoinGoldFieldResult();
        //检查用户金币是否在金币场的范围内
        GoldField goldField = goldFieldRepository.find(goldFieldId);
        boolean balanceInRange = GameCurrencyAccountingService.isBalanceInRange(this,
                userId, "gold", goldField.getMinGold() + "", goldField.getMaxGold() + "");
        if (!balanceInRange) {
            result.setBalanceNotInRange(true);
            return result;
        }
        //请求匹配
        RandomMatchService.requestMatch(this,
                String.valueOf(userId), goldFieldId, currentTime, 10 * 1000);
        result.setSuccess(true);
        return result;
    }

    @Process
    public void cancelGoldFieldMatch(long userId, String goldFieldId) {
        RandomMatchService.cancelMatch(this,
                String.valueOf(userId), goldFieldId);
    }

    @Process
    public List<String> matchGoldField(String goldFieldId, long currentTime) {
        boolean startSuccess = RandomMatchService.startMatch(this,
                goldFieldId, 20 * 1000, currentTime);
        if (!startSuccess) {
            return null;
        }

        List<String> allMatchPlayerIds = goldFieldRandomMatchPlayerRepository.getAllMatchPlayerIds(goldFieldId);
        List<RandomMatchPlayer> allPlayersToMatch = new ArrayList<>();
        for (String playerId : allMatchPlayerIds) {
            RandomMatchPlayer player = RandomMatchService.takePlayerToMatch(this,
                    playerId, goldFieldId);
            if (player != null) {
                allPlayersToMatch.add(player);
            }
        }

        RandomMatchResult matchResult = RandomMatchService.match(this,
                goldFieldId, allPlayersToMatch, 4, currentTime, 10 * 1000);
        return matchResult.getNotMatchedPlayers();
    }

    @Process
    public List<String> acquireMatchedPlayers(String goldFieldId, long currentTime) {
        return RandomMatchService.acquireMatchedPlayers(this,
                goldFieldId, currentTime);
    }

    @Process
    public String acquireMatchFailurePlayerId(String goldFieldId, long currentTime) {
        return RandomMatchService.acquireMatchFailedPlayer(this,
                goldFieldId, currentTime);
    }


    public void deductGold(List<Long> userIdList, String goldFieldId) {
        GoldField goldField = goldFieldRepository.find(goldFieldId);
        for (long userId : userIdList) {
            long billItemId = generateBillItemId();
            withdrawGold(userId, goldField.getDifen() + "", billItemId);
        }
    }

    @Process
    private long generateBillItemId() {
        return gameCurrencyAccountBillItemIdGeneratorRepository.take().generateId();
    }

    @Process
    private void withdrawGold(long userId, String amount, long billItemId) {
        MoneyBillItem moneyBillItem = new MoneyBillItem();
        moneyBillItem.setId(billItemId);
        moneyBillItem.setReason("gold_field");
        GameCurrencyAccountingService.withdraw(this,
                userId, "gold", amount, moneyBillItem);
    }

    @Override
    public GameCurrencyAccountRepository getGameCurrencyAccountRepository() {
        return moneyAccountRepository;
    }

    @Override
    public GameCurrencyAccountIdGeneratorRepository getGameCurrencyAccountIdGeneratorRepository() {
        return gameCurrencyAccountIdGeneratorRepository;
    }

    @Override
    public GameUserCurrencyAccountsRepository getGameUserCurrencyAccountsRepository() {
        return gameUserCurrencyAccountsRepository;
    }

    @Override
    public GameCurrencyAccountBillItemRepository getGameCurrencyAccountBillItemRepository() {
        return moneyBillItemRepository;
    }

    @Override
    public MatchStateRepository getMatchStateRepository() {
        return goldFieldMatchStateRepository;
    }

    @Override
    public RandomMatchPlayerRepository getRandomMatchPlayerRepository() {
        return goldFieldRandomMatchPlayerRepository;
    }

    @Override
    public MatchResultAcquisitionTaskRepository getMatchResultAcquisitionTaskRepository() {
        return goldFieldMatchResultAcquisitionTaskRepository;
    }

    @Override
    public MatchResultAcquisitionTaskSegmentRepository getMatchResultAcquisitionTaskSegmentRepository() {
        return goldFieldMatchResultAcquisitionTaskSegmentRepository;
    }

    @Override
    public MatchResultAcquisitionTaskSegmentIDGeneratorRepository getMatchResultAcquisitionTaskSegmentIDGeneratorRepository() {
        return goldFieldMatchResultAcquisitionTaskSegmentIDGeneratorRepository;
    }

    @Override
    public PlayerMatchFailureAcquisitionTaskRepository getPlayerMatchFailureAcquisitionTaskRepository() {
        return goldFieldPlayerMatchFailureAcquisitionTaskRepository;
    }

    @Override
    public PlayerMatchFailureAcquisitionTaskSegmentRepository getPlayerMatchFailureAcquisitionTaskSegmentRepository() {
        return goldFieldPlayerMatchFailureAcquisitionTaskSegmentRepository;
    }

    @Override
    public PlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository getPlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository() {
        return goldFieldPlayerMatchFailureAcquisitionTaskSegmentIDGeneratorRepository;
    }

    @Override
    public PlayerCurrentMatchRepository getPlayerCurrentMatchRepository() {
        return playerCurrentMatchRepository;
    }


}
