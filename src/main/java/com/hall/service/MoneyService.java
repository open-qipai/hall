package com.hall.service;

import com.hall.entity.money.MoneyAccount;
import com.hall.entity.money.MoneyBillItem;
import com.hall.repository.GameCurrencyAccountBillItemIdGeneratorRepository;
import com.hall.repository.HallUserRepository;
import com.hall.repository.MoneyAccountRepository;
import com.hall.repository.MoneyBillItemRepository;
import com.hall.repository.impl.MongodbHallGameUserCurrencyAccountsRepository;
import dml.gamecurrency.entity.GameUserCurrencyAccounts;
import dml.gamecurrency.entity.UserInitiateMoneyTask;
import dml.gamecurrency.entity.UserInitiateMoneyTaskSegment;
import dml.gamecurrency.repository.*;
import dml.gamecurrency.service.GameCurrencyAccountingService;
import dml.gamecurrency.service.UserInitiateMoneyService;
import dml.gamecurrency.service.repositoryset.GameCurrencyAccountingServiceRepositorySet;
import dml.gamecurrency.service.repositoryset.UserInitiateMoneyServiceRepositorySet;
import dml.id.entity.UUIDGenerator;
import erp.annotation.Process;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class MoneyService implements
        GameCurrencyAccountingServiceRepositorySet,
        UserInitiateMoneyServiceRepositorySet {

    @Autowired
    private MoneyAccountRepository moneyAccountRepository;
    @Autowired
    private GameCurrencyAccountIdGeneratorRepository gameCurrencyAccountIdGeneratorRepository;
    @Autowired
    private MongodbHallGameUserCurrencyAccountsRepository gameUserCurrencyAccountsRepository;
    @Autowired
    private MoneyBillItemRepository moneyBillItemRepository;
    @Autowired
    private GameCurrencyAccountBillItemIdGeneratorRepository gameCurrencyAccountBillItemIdGeneratorRepository;
    @Autowired
    private HallUserRepository hallUserRepository;
    private UserInitiateMoneyTaskRepository userInitiateMoneyTaskRepository;
    private UserInitiateMoneyTaskSegmentRepository userInitiateMoneyTaskSegmentRepository;
    private UserInitiateMoneyTaskSegmentIDGeneratorRepository userInitiateMoneyTaskSegmentIDGeneratorRepository;

    @Autowired
    public MoneyService(MongoTemplate mongoTemplate, RedisTemplate redisTemplate) {
        userInitiateMoneyTaskRepository = RepositoryFactory.newInstance(UserInitiateMoneyTaskRepository.class,
                new RedisRepository(redisTemplate, UserInitiateMoneyTask.class));
        userInitiateMoneyTaskSegmentRepository = RepositoryFactory.newInstance(UserInitiateMoneyTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, UserInitiateMoneyTaskSegment.class));
        userInitiateMoneyTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(UserInitiateMoneyTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "userInitiateMoneyTaskSegmentIDGeneratorRepository"));
    }

    /**
     * 给所有用户发初始金币。
     */
    public void sendAllUserInitGold(int initGold, long currentTime) {
        boolean taskCompleted = UserInitiateMoneyService.isUserInitiateMoneyTaskCompleted(this,
                "initGold");
        if (taskCompleted) {
            return;
        }

        createUserInitiateMoneyTask(currentTime);

        while (!taskCompleted) {
            executeUserInitiateMoneyTask(System.currentTimeMillis(), initGold);
            taskCompleted = UserInitiateMoneyService.isUserInitiateMoneyTaskCompleted(this,
                    "initGold");
        }
    }


    public Long executeUserInitiateMoneyTask(long currentTime, int initGold) {
        return doExecuteUserInitiateMoneyTask(currentTime, initGold, generateNewBillItemId());
    }

    @Process
    private Long doExecuteUserInitiateMoneyTask(long currentTime, int initGold, long newBillItemId) {
        long maxSegmentExecutionTime = 60 * 1000;
        long maxTimeToTaskReady = 60 * 1000;
        Long userId = (Long) UserInitiateMoneyService.executeUserInitiateMoneyTask(this,
                "initGold", currentTime, maxSegmentExecutionTime, maxTimeToTaskReady,
                new MoneyAccount(), new MoneyBillItem(newBillItemId), "gold", initGold + "");
        return userId;
    }

    @Process
    private long generateNewBillItemId() {
        return gameCurrencyAccountBillItemIdGeneratorRepository.take().generateId();
    }

    @Process
    public void createUserInitiateMoneyTask(long currentTime) {
        boolean createTaskSuccess = UserInitiateMoneyService.createUserInitiateMoneyTask(this,
                "initGold", currentTime);
        if (createTaskSuccess) {
            List<Long> allUserID = hallUserRepository.getAllUserID();
            UserInitiateMoneyService.addAllUserIdToUserInitiateMoneyTask(this,
                    "initGold", 1000, allUserID);
        }
    }


    public void sendGold(long userId, int gold, String reason) {
        doSendGold(userId, gold, reason, generateNewBillItemId());
    }

    @Process
    public void batchSendGold(List<Long> hallUserIdList, int initGold, String reason) {
        for (Long userId : hallUserIdList) {
            long newBillItemId = gameCurrencyAccountBillItemIdGeneratorRepository.take().generateId();
            MoneyBillItem moneyBillItem = new MoneyBillItem(newBillItemId);
            moneyBillItem.setReason(reason);
            GameCurrencyAccountingService.deposit(this,
                    userId, "gold", initGold + "", new MoneyAccount(), moneyBillItem);
        }
    }

    @Process
    public void doSendGold(long userId, int gold, String reason, long newBillItemId) {
        MoneyBillItem moneyBillItem = new MoneyBillItem(newBillItemId);
        moneyBillItem.setReason(reason);
        GameCurrencyAccountingService.deposit(this,
                userId, "gold", gold + "", new MoneyAccount(), moneyBillItem);
    }

    public int getGold(long userId) {
        MoneyAccount moneyAccount = (MoneyAccount) GameCurrencyAccountingService.getAccount(this, userId, "gold");
        return moneyAccount == null ? 0 : moneyAccount.getBalance() == null ? 0 : Integer.valueOf(moneyAccount.getBalance());
    }

    public MoneyAccount getGoldAccount(long userId) {
        return (MoneyAccount) GameCurrencyAccountingService.getAccount(this, userId, "gold");
    }

    public List<MoneyAccount> getAllMoneyAccountForUser(long userId) {
        return (List) GameCurrencyAccountingService.getAllAccountsForUser(this, userId);
    }

    public List<MoneyAccount> findMoneyAccountForUsers(List<Long> matchedPlayerIdList, String currencyName) {
        List<GameUserCurrencyAccounts> gameUserCurrencyAccountsList = gameUserCurrencyAccountsRepository.
                findGameUserCurrencyAccountsByUserIds(matchedPlayerIdList);
        Map<Object, GameUserCurrencyAccounts> gameUserCurrencyAccountsMap = gameUserCurrencyAccountsList.stream().collect(
                java.util.stream.Collectors.toMap(GameUserCurrencyAccounts::getUserId, (p) -> p));
        List<GameUserCurrencyAccounts> ordereGameUserCurrencyAccountsList =
                matchedPlayerIdList.stream().map((userId) -> gameUserCurrencyAccountsMap.get(userId)).collect(java.util.stream.Collectors.toList());
        List<Object> moneyAccountIdList = ordereGameUserCurrencyAccountsList.stream().map((p) -> p.getAccount(currencyName)).
                collect(java.util.stream.Collectors.toList());
        List<MoneyAccount> moneyAccountList = moneyAccountRepository.findMoneyAccountByIds(moneyAccountIdList);
        Map<Object, MoneyAccount> moneyAccountMap = moneyAccountList.stream().collect(
                java.util.stream.Collectors.toMap(MoneyAccount::getId, (p) -> p));
        List<MoneyAccount> orderedMoneyAccountList = moneyAccountIdList.stream().map((moneyAccountId) -> moneyAccountMap.get(moneyAccountId)).
                collect(java.util.stream.Collectors.toList());
        return orderedMoneyAccountList;
    }

    @Override
    public GameCurrencyAccountRepository getGameCurrencyAccountRepository() {
        return moneyAccountRepository;
    }

    @Override
    public GameCurrencyAccountIdGeneratorRepository getGameCurrencyAccountIdGeneratorRepository() {
        return gameCurrencyAccountIdGeneratorRepository;
    }

    @Override
    public GameUserCurrencyAccountsRepository getGameUserCurrencyAccountsRepository() {
        return gameUserCurrencyAccountsRepository;
    }

    @Override
    public GameCurrencyAccountBillItemRepository getGameCurrencyAccountBillItemRepository() {
        return moneyBillItemRepository;
    }

    @Override
    public UserInitiateMoneyTaskRepository getUserInitiateMoneyTaskRepository() {
        return userInitiateMoneyTaskRepository;
    }

    @Override
    public UserInitiateMoneyTaskSegmentRepository getUserInitiateMoneyTaskSegmentRepository() {
        return userInitiateMoneyTaskSegmentRepository;
    }

    @Override
    public UserInitiateMoneyTaskSegmentIDGeneratorRepository getUserInitiateMoneyTaskSegmentIDGeneratorRepository() {
        return userInitiateMoneyTaskSegmentIDGeneratorRepository;
    }


}
