package com.hall.service;

import com.hall.entity.user.HallUserSession;
import com.hall.repository.HallUserSessionRepository;
import com.hall.repository.UserSessionIDGeneratorRepository;
import dml.user.entity.UserCurrentSession;
import dml.user.repository.UserCurrentSessionRepository;
import dml.user.repository.UserIDGeneratorRepository;
import erp.annotation.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.hash.HashMapper;
import org.springframework.data.redis.hash.ObjectHashMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class TestService {
    @Autowired
    private HallUserSessionRepository hallUserSessionRepository;
    @Autowired
    private UserSessionIDGeneratorRepository userSessionIDGeneratorRepository;
    @Autowired
    private UserCurrentSessionRepository hallUserCurrentSessionRepository;
    @Autowired
    private UserIDGeneratorRepository userIDGeneratorRepository;
    @Autowired
    private RedisTemplate redisTemplate;

    private List<String> allSessionIDs;

    private Random random = new Random();

    private HashMapper<Object, byte[], byte[]> mapper = new ObjectHashMapper();

    @PostConstruct
    public void init() {
        allSessionIDs = hallUserSessionRepository.getAllSessionId();
    }

    @Process
    public void createSession() {
        HallUserSession hallUserSession = new HallUserSession();
        hallUserSession.setId(userSessionIDGeneratorRepository.take().generateId());
        hallUserSession.setUserID(1L);
        hallUserSessionRepository.put(hallUserSession);
        allSessionIDs.add(hallUserSession.getId());
    }

    public HallUserSession querySession() {
        String sessionID = allSessionIDs.get(random.nextInt(allSessionIDs.size()));
        return hallUserSessionRepository.find(sessionID);
    }


    public UserCurrentSession register() {
        String sessionID = newSessionID();
        return doRegister(sessionID);
    }

    @Process
    private UserCurrentSession doRegister(String sessionID) {
        long userId = (long) userIDGeneratorRepository.take().generateId();
        UserCurrentSession userCurrentSession = new UserCurrentSession();
        userCurrentSession.setUserID(userId);
        userCurrentSession.setCurrentSessionID(sessionID);
        hallUserCurrentSessionRepository.put(userCurrentSession);
        return userCurrentSession;
    }

    @Process
    private String newSessionID() {
        return userSessionIDGeneratorRepository.take().generateId();
    }


    public HallUserSession login(long userId) {
        String newSessionID = newSessionID();
        return doLogin(userId, newSessionID);
    }

    @Process
    private HallUserSession doLogin(long userId, String newSessionID) {
        HallUserSession newSession = new HallUserSession(newSessionID);
        newSession.setUserID(userId);
        hallUserSessionRepository.put(newSession);

        UserCurrentSession userCurrentSession = hallUserCurrentSessionRepository.take(userId);
        String currentSessionID = userCurrentSession.getCurrentSessionID();
        hallUserSessionRepository.remove(currentSessionID);
        userCurrentSession.setCurrentSessionID(newSessionID);
//
//        allSessionIDs.add(newSessionID);
//        allSessionIDs.remove(currentSessionID);
        return newSession;
    }

    public void removeSession() {
        long t01 = System.currentTimeMillis();
        String sessionID = takeSessionID();
        long t02 = System.currentTimeMillis();
        String key = "repository:" + "HallUserSession" + ":" + sessionID;
        String mutexesKey = "mutexes:HallUserSession" + ":" + sessionID;
        String processName = "com.hall.service.TestService.removeSession";

        long t1 = System.currentTimeMillis();
//        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
//        String lock = (String) valueOperations.get(mutexesKey);
        long t2 = System.currentTimeMillis();

        long t3 = System.currentTimeMillis();
//        HashOperations<String, byte[], byte[]> hashOperations = redisTemplate.opsForHash();
//        Map<byte[], byte[]> loadedHash = hashOperations.entries(key);
//        HallUserSession session = (HallUserSession) mapper.fromHash(loadedHash);
        long t4 = System.currentTimeMillis();

        long t5 = System.currentTimeMillis();
//        boolean set = valueOperations.setIfAbsent(mutexesKey, processName, 30000L, TimeUnit.MILLISECONDS);
        long t6 = System.currentTimeMillis();

        long t7 = System.currentTimeMillis();
        List<String> strIdList = new ArrayList<>();
        strIdList.add(key);
        redisTemplate.delete(strIdList);
        long t8 = System.currentTimeMillis();

        long t9 = System.currentTimeMillis();
//        List<String> strIdList1 = new ArrayList<>();
//        strIdList1.add(mutexesKey);
//        redisTemplate.delete(strIdList1);
        long t10 = System.currentTimeMillis();

        long t11 = System.currentTimeMillis();
//        List<String> strIdList2 = new ArrayList<>();
//        strIdList2.add(mutexesKey);
//        redisTemplate.delete(strIdList2);
        long t12 = System.currentTimeMillis();

        System.out.println("t2-t1: " + (t2 - t1) + ", t4-t3: " + (t4 - t3) + ", t6-t5: " + (t6 - t5) +
                ", t8-t7: " + (t8 - t7) + ", t10-t9: " + (t10 - t9) + ", t12-t11: " + (t12 - t11) + ", total: " + (t12 - t1)
                + ", key: " + (t02 - t01));
    }

//    @Process
//    public void removeSession() {
//        String sessionID = takeSessionID();
//        hallUserSessionRepository.remove(sessionID);
//    }

    private synchronized String takeSessionID() {
        return allSessionIDs.remove(random.nextInt(allSessionIDs.size()));
    }

}
