package com.hall.service;

import com.hall.repository.HallUserSessionRepository;
import dml.id.entity.UUIDGenerator;
import dml.user.entity.ClearSessionTask;
import dml.user.entity.ClearSessionTaskSegment;
import dml.user.repository.*;
import dml.user.service.UserSessionCleanupTaskService;
import dml.user.service.repositoryset.UserSessionCleanupServiceRepositorySet;
import dml.user.service.repositoryset.UserSessionCleanupTaskServiceRepositorySet;
import erp.annotation.Process;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClearSessionService implements
        UserSessionCleanupServiceRepositorySet,
        UserSessionCleanupTaskServiceRepositorySet {

    private ClearSessionTaskRepository hallUserClearSessionTaskRepository;
    private ClearSessionTaskSegmentRepository hallUserClearSessionTaskSegmentRepository;
    private ClearSessionTaskSegmentIDGeneratorRepository clearSessionTaskSegmentIDGeneratorRepository;
    @Autowired
    private HallUserSessionRepository hallUserSessionRepository;
    @Autowired
    private UserSessionAliveKeeperRepository hallUserSessionAliveKeeperRepository;

    @Autowired
    public ClearSessionService(RedisTemplate redisTemplate) {
        hallUserClearSessionTaskRepository = RepositoryFactory.newInstance(ClearSessionTaskRepository.class,
                new RedisRepository(redisTemplate, ClearSessionTask.class, "hallUserClearSessionTaskRepository"));
        hallUserClearSessionTaskSegmentRepository = RepositoryFactory.newInstance(ClearSessionTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, ClearSessionTaskSegment.class, "hallUserClearSessionTaskSegmentRepository"));
        clearSessionTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(ClearSessionTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "clearSessionTaskSegmentIDGeneratorRepository"));
    }

    @Process
    public boolean createUserSessionCleanupTask(long currentTime) {
        String taskName = "ClearSessionTask";
        return UserSessionCleanupTaskService.createUserSessionCleanupTask(this,
                taskName, currentTime);
    }

    @Process
    public void addAllSessionIdToUserSessionCleanupTask(int sessionBatchSize) {
        String taskName = "ClearSessionTask";
        List<String> allSessionId = hallUserSessionRepository.getAllSessionId();
        UserSessionCleanupTaskService.addAllSessionIdToUserSessionCleanupTask(this,
                taskName, sessionBatchSize, allSessionId);
    }

    @Process
    public String takeUserSessionCleanupTaskSegmentToExecute(long currentTime,
                                                             long maxSegmentExecutionTime,
                                                             long maxTimeToTaskReady) {
        String taskName = "ClearSessionTask";
        return UserSessionCleanupTaskService.takeUserSessionCleanupTaskSegmentToExecute(this,
                taskName, currentTime, maxSegmentExecutionTime, maxTimeToTaskReady);

    }

    @Process
    public void executeUserSessionCleanupTask(String segmentId,
                                              long currentTime,
                                              long sessionKeepAliveInterval) {
        UserSessionCleanupTaskService.executeUserSessionCleanupTaskSegment(this,
                segmentId, currentTime, sessionKeepAliveInterval);
    }

    @Override
    public UserSessionRepository getUserSessionRepository() {
        return hallUserSessionRepository;
    }

    @Override
    public UserSessionAliveKeeperRepository getUserSessionAliveKeeperRepository() {
        return hallUserSessionAliveKeeperRepository;
    }

    @Override
    public ClearSessionTaskRepository getClearSessionTaskRepository() {
        return hallUserClearSessionTaskRepository;
    }

    @Override
    public ClearSessionTaskSegmentRepository getClearSessionTaskSegmentRepository() {
        return hallUserClearSessionTaskSegmentRepository;
    }


    @Override
    public ClearSessionTaskSegmentIDGeneratorRepository getClearSessionTaskSegmentIDGeneratorRepository() {
        return clearSessionTaskSegmentIDGeneratorRepository;
    }


}
