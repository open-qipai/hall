package com.hall.service.gm;

import com.hall.entity.gm.GMPermission;
import com.hall.entity.gm.GMRole;
import com.hall.repository.*;
import com.hall.repository.impl.*;
import dml.id.entity.UUIDGenerator;
import dml.rbac.entity.UserPermissions;
import dml.rbac.entity.UserRoles;
import dml.rbac.entity.cleanup.RolePermissionCleanupTaskSegment;
import dml.rbac.entity.cleanup.UserPermissionCleanupTaskSegment;
import dml.rbac.entity.cleanup.UserRoleCleanupTaskSegment;
import dml.rbac.repository.*;
import dml.rbac.service.RbacService;
import dml.rbac.service.repositoryset.RbacServiceRepositorySet;
import erp.annotation.Process;
import erp.mongodb.MongodbRepository;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class GMRbacService implements RbacServiceRepositorySet {

    private GMPermissionRepository gmPermissionRepository;
    private GMRoleRepository gmRoleRepository;
    private RolePermissionCleanupTaskSegmentIDGeneratorRepository rolePermissionCleanupTaskSegmentIDGeneratorRepository;
    private UserRoleCleanupTaskSegmentIDGeneratorRepository userRoleCleanupTaskSegmentIDGeneratorRepository;
    private UserPermissionCleanupTaskSegmentIDGeneratorRepository userPermissionCleanupTaskSegmentIDGeneratorRepository;
    private GmRolePermissionCleanupTaskRepository gmRolePermissionCleanupTaskRepository;
    private RolePermissionCleanupTaskSegmentRepository gmRolePermissionCleanupTaskSegmentRepository;
    private GmUserRoleCleanupTaskRepository gmUserRoleCleanupTaskRepository;
    private UserRoleCleanupTaskSegmentRepository gmUserRoleCleanupTaskSegmentRepository;
    private GmUserPermissionCleanupTaskRepository gmUserPermissionCleanupTaskRepository;
    private UserPermissionCleanupTaskSegmentRepository gmUserPermissionCleanupTaskSegmentRepository;
    private UserPermissionsRepository gmUserPermissionsRepository;
    private UserRolesRepository gmUserRolesRepository;
    @Autowired
    private GMUserRepository gmUserRepository;

    @Autowired
    public GMRbacService(MongoTemplate mongoTemplate, RedisTemplate redisTemplate) {
        gmPermissionRepository = new ViewCachedMongodbGMPermissionRepository(mongoTemplate);
        gmRoleRepository = new ViewCachedMongodbGMRoleRepository(mongoTemplate);
        rolePermissionCleanupTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(RolePermissionCleanupTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "rolePermissionCleanupTaskSegmentIDGeneratorRepository"));
        userRoleCleanupTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(UserRoleCleanupTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "userRoleCleanupTaskSegmentIDGeneratorRepository"));
        userPermissionCleanupTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(UserPermissionCleanupTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "userPermissionCleanupTaskSegmentIDGeneratorRepository"));
        gmRolePermissionCleanupTaskRepository = new RedisGmRolePermissionCleanupTaskRepository(redisTemplate);
        gmRolePermissionCleanupTaskSegmentRepository = RepositoryFactory.newInstance(RolePermissionCleanupTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, RolePermissionCleanupTaskSegment.class, "gmRolePermissionCleanupTaskSegmentRepository"));
        gmUserRoleCleanupTaskRepository = new RedisGmUserRoleCleanupTaskRepository(redisTemplate);
        gmUserRoleCleanupTaskSegmentRepository = RepositoryFactory.newInstance(UserRoleCleanupTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, UserRoleCleanupTaskSegment.class, "gmUserRoleCleanupTaskSegmentRepository"));
        gmUserPermissionCleanupTaskRepository = new RedisGmUserPermissionCleanupTaskRepository(redisTemplate);
        gmUserPermissionCleanupTaskSegmentRepository = RepositoryFactory.newInstance(UserPermissionCleanupTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, UserPermissionCleanupTaskSegment.class, "gmUserPermissionCleanupTaskSegmentRepository"));
        gmUserPermissionsRepository = RepositoryFactory.newInstance(UserPermissionsRepository.class,
                new MongodbRepository(mongoTemplate, UserPermissions.class, "gmUserPermissionsRepository"));
        gmUserRolesRepository = RepositoryFactory.newInstance(UserRolesRepository.class,
                new MongodbRepository(mongoTemplate, UserRoles.class, "gmUserRolesRepository"));

    }

    public List<GMPermission> getPermissionList(int from, int size) {
        return gmPermissionRepository.list(from, size);
    }

    public int countPermissions() {
        return gmPermissionRepository.countAll();
    }

    @Process
    public GMPermission removePermission(String id) {
        return (GMPermission) RbacService.deletePermission(this, id);
    }

    public GMPermission getPermission(String id) {
        return gmPermissionRepository.find(id);
    }

    @Process
    public GMPermission addPermission(String id, String name) {
        GMPermission permission = new GMPermission();
        permission.setId(id);
        permission.setName(name);
        gmPermissionRepository.put(permission);
        return permission;
    }

    @Process
    public GMPermission modifyPermission(String id, String name) {
        GMPermission permission = gmPermissionRepository.take(id);
        if (permission == null) {
            return null;
        }
        permission.setName(name);
        return permission;
    }

    public List<GMRole> getRoleList(int from, int size) {
        return gmRoleRepository.list(from, size);
    }

    public int countRoles() {
        return gmRoleRepository.countAll();
    }

    @Process
    public GMRole removeRole(String id) {
        return (GMRole) RbacService.deleteRole(this, id);
    }

    public GMRole getRole(String id) {
        return gmRoleRepository.find(id);
    }

    public List<GMPermission> getAllPermissionList() {
        return gmPermissionRepository.listAll();
    }

    @Process
    public GMRole addRole(String id, String name, Set<String> permissions) {
        GMRole newRole = new GMRole();
        newRole.setName(name);
        newRole.setPermissions(permissions);
        return (GMRole) RbacService.createRole(this, id, newRole);
    }

    @Process
    public GMRole modifyRole(String id, String name, List<String> permissionIdList) {
        GMRole gmRole = gmRoleRepository.take(id);
        if (gmRole == null) {
            return null;
        }
        gmRole.setName(name);
        gmRole = (GMRole) RbacService.setPermissionsToRole(this, id,
                permissionIdList == null ? null : Set.copyOf(permissionIdList));
        return gmRole;
    }

    public Set<String> getUserPermissions(String account) {
        UserPermissions userPermissions = gmUserPermissionsRepository.find(account);
        return userPermissions == null ? null : userPermissions.getPermissionIds();
    }

    public Set<String> getUserRolePermissions(String account) {
        Set<String> userRolePermissions = new HashSet<>();
        UserRoles userRoles = gmUserRolesRepository.find(account);
        if (userRoles == null) {
            return null;
        }
        for (String roleId : userRoles.getRoleIds()) {
            GMRole gmRole = gmRoleRepository.find(roleId);
            if (gmRole != null) {
                userRolePermissions.addAll(gmRole.getPermissions());
            }
        }
        return userRolePermissions;
    }

    @Process
    public void saveUserPermissions(String account, List<String> permissions) {
        RbacService.setPermissionsToUser(this, account, permissions == null ? new HashSet<>() : Set.copyOf(permissions));
    }

    public Set<String> getUserRoles(String account) {
        UserRoles userRoles = gmUserRolesRepository.find(account);
        return userRoles == null ? null : userRoles.getRoleIds();
    }

    public List<GMRole> getAllRoleList() {
        return gmRoleRepository.listAll();
    }

    @Process
    public void saveUserRoles(String account, List<String> roles) {
        RbacService.setRolesToUser(this, account, roles == null ? new HashSet<>() : Set.copyOf(roles));
    }

    @Process
    public void createRolePermissionCleanupTask(String permissionId, long currentTime) {
        List<String> allRoleId = gmRoleRepository.listAllId();
        RbacService.createRolePermissionCleanupTask(this,
                "RolePermissionCleanupTask_" + permissionId, permissionId, allRoleId, currentTime);
    }

    @Process
    public void createUserRoleCleanupTask(String roleId, long currentTime) {
        List<String> allAccount = gmUserRepository.listAllId();
        RbacService.createUserRoleCleanupTask(this,
                "UserRoleCleanupTask_" + roleId, roleId, allAccount, currentTime);
    }

    public List<String> getAllRolePermissionCleanupTaskName() {
        return gmRolePermissionCleanupTaskRepository.listAllName();
    }

    public List<String> getAllUserRoleCleanupTaskName() {
        return gmUserRoleCleanupTaskRepository.listAllName();
    }

    public List<String> getAllUserPermissionCleanupTaskName() {
        return gmUserPermissionCleanupTaskRepository.listAllName();
    }

    @Process
    public boolean executeUserRoleCleanupTask(String taskName, long currentTime) {
        return RbacService.executeUserRoleCleanupTask(this,
                taskName, currentTime, 1000 * 10, 1000 * 60);
    }

    @Process
    public boolean executeRolePermissionCleanupTask(String taskName, long currentTime) {
        return RbacService.executeRolePermissionCleanupTask(this,
                taskName, currentTime, 1000 * 10, 1000 * 60);
    }

    @Process
    public void createUserPermissionCleanupTask(String permissionId, long currentTime) {
        List<String> allAccount = gmUserRepository.listAllId();
        RbacService.createUserPermissionCleanupTask(this,
                "UserPermissionCleanupTask_" + permissionId, permissionId, allAccount, currentTime);
    }

    @Process
    public boolean executeUserPermissionCleanupTask(String taskName, long currentTime) {
        return RbacService.executeUserPermissionCleanupTask(this,
                taskName, currentTime, 1000 * 10, 1000 * 60);
    }

    @Process
    public boolean hasPermission(String gmAccount, String permissionId) {
        return RbacService.hasPermission(this, gmAccount, permissionId);
    }

    public List<String> getAllPermissions(String gmAccount) {
        Set<String> allPermissionIds = RbacService.getAllPermissions(this, gmAccount);
        return allPermissionIds == null ? null : List.copyOf(allPermissionIds);
    }

    @Override
    public UserPermissionsRepository getUserPermissionsRepository() {
        return gmUserPermissionsRepository;
    }

    @Override
    public PermissionRepository getPermissionRepository() {
        return gmPermissionRepository;
    }

    @Override
    public RoleRepository getRoleRepository() {
        return gmRoleRepository;
    }

    @Override
    public UserRolesRepository getUserRolesRepository() {
        return gmUserRolesRepository;
    }

    @Override
    public RolePermissionCleanupTaskRepository getRolePermissionCleanupTaskRepository() {
        return gmRolePermissionCleanupTaskRepository;
    }

    @Override
    public RolePermissionCleanupTaskSegmentRepository getRolePermissionCleanupTaskSegmentRepository() {
        return gmRolePermissionCleanupTaskSegmentRepository;
    }

    @Override
    public RolePermissionCleanupTaskSegmentIDGeneratorRepository getRolePermissionCleanupTaskSegmentIDGeneratorRepository() {
        return rolePermissionCleanupTaskSegmentIDGeneratorRepository;
    }

    @Override
    public UserRoleCleanupTaskRepository getUserRoleCleanupTaskRepository() {
        return gmUserRoleCleanupTaskRepository;
    }

    @Override
    public UserRoleCleanupTaskSegmentRepository getUserRoleCleanupTaskSegmentRepository() {
        return gmUserRoleCleanupTaskSegmentRepository;
    }

    @Override
    public UserRoleCleanupTaskSegmentIDGeneratorRepository getUserRoleCleanupTaskSegmentIDGeneratorRepository() {
        return userRoleCleanupTaskSegmentIDGeneratorRepository;
    }

    @Override
    public UserPermissionCleanupTaskRepository getUserPermissionCleanupTaskRepository() {
        return gmUserPermissionCleanupTaskRepository;
    }

    @Override
    public UserPermissionCleanupTaskSegmentRepository getUserPermissionCleanupTaskSegmentRepository() {
        return gmUserPermissionCleanupTaskSegmentRepository;
    }

    @Override
    public UserPermissionCleanupTaskSegmentIDGeneratorRepository getUserPermissionCleanupTaskSegmentIDGeneratorRepository() {
        return userPermissionCleanupTaskSegmentIDGeneratorRepository;
    }


}
