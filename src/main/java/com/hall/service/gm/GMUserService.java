package com.hall.service.gm;

import com.hall.entity.gm.GMUser;
import com.hall.entity.gm.GMUserSession;
import com.hall.repository.AdminUserSessionIDGeneratorRepository;
import com.hall.repository.GMUserRepository;
import com.hall.repository.GMUserSessionRepository;
import com.hall.repository.impl.RedisGMUserSessionRepository;
import dml.adminuser.entity.AdminUserCurrentSession;
import dml.adminuser.entity.AdminUserSessionAliveKeeper;
import dml.adminuser.entity.ClearSessionTakeSegment;
import dml.adminuser.entity.ClearSessionTask;
import dml.adminuser.repository.*;
import dml.adminuser.service.AdminUserService;
import dml.adminuser.service.repositoryset.AdminUserServiceRepositorySet;
import dml.adminuser.service.result.AddAdminUserResult;
import dml.adminuser.service.result.LoginResult;
import dml.id.entity.UUIDGenerator;
import dml.id.entity.UUIDStyleRandomStringIdGenerator;
import erp.annotation.Process;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GMUserService implements AdminUserServiceRepositorySet {
    @Autowired
    private GMUserRepository gmUserRepository;
    private GMUserSessionRepository gmUserSessionRepository;
    private AdminUserSessionIDGeneratorRepository adminUserSessionIDGeneratorRepository;
    private AdminUserCurrentSessionRepository gmAdminUserCurrentSessionRepository;
    private AdminUserSessionAliveKeeperRepository gmAdminUserSessionAliveKeeperRepository;
    private ClearSessionTaskRepository gmClearSessionTaskRepository;
    private ClearSessionTaskSegmentRepository gmClearSessionTaskSegmentRepository;
    private ClearSessionTaskSegmentIDGeneratorRepository clearSessionTaskSegmentIDGeneratorRepository;

    @Autowired
    public GMUserService(MongoTemplate mongoTemplate, RedisTemplate redisTemplate) {
        gmUserSessionRepository = new RedisGMUserSessionRepository(redisTemplate);
        adminUserSessionIDGeneratorRepository = SingletonRepositoryFactory.newInstance(AdminUserSessionIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDStyleRandomStringIdGenerator(), "adminUserSessionIDGeneratorRepository"));
        gmAdminUserCurrentSessionRepository = RepositoryFactory.newInstance(AdminUserCurrentSessionRepository.class,
                new RedisRepository(redisTemplate, AdminUserCurrentSession.class, "gmAdminUserCurrentSessionRepository"));
        gmAdminUserSessionAliveKeeperRepository = RepositoryFactory.newInstance(AdminUserSessionAliveKeeperRepository.class,
                new RedisRepository(redisTemplate, AdminUserSessionAliveKeeper.class, "gmAdminUserSessionAliveKeeperRepository"));
        gmClearSessionTaskRepository = RepositoryFactory.newInstance(ClearSessionTaskRepository.class,
                new RedisRepository(redisTemplate, ClearSessionTask.class, "gmClearSessionTaskRepository"));
        gmClearSessionTaskSegmentRepository = RepositoryFactory.newInstance(ClearSessionTaskSegmentRepository.class,
                new RedisRepository(redisTemplate, ClearSessionTakeSegment.class, "gmClearSessionTaskSegmentRepository"));
        clearSessionTaskSegmentIDGeneratorRepository = SingletonRepositoryFactory.newInstance(ClearSessionTaskSegmentIDGeneratorRepository.class,
                new MemSingletonRepository(new UUIDGenerator(), "gmClearSessionTaskSegmentIDGeneratorRepository"));
    }

    public List<GMUser> getGMUserList(String account, String name, int from, int size) {
        return gmUserRepository.getGMUserList(account, name, from, size);
    }

    public GMUser getGMUserByAccount(String account) {
        return gmUserRepository.find(account);
    }

    @Process
    public boolean addGMUser(String account, String password, String name, long currentTime) {
        GMUser gmUser = new GMUser();
        gmUser.setName(name);
        gmUser.setCreateTime(currentTime);
        AddAdminUserResult addAdminUserResult = AdminUserService.addAdminUser(this,
                account, password, gmUser);
        return addAdminUserResult.isAlreadyExist();
    }

    public LoginResult login(String account, String password) {
        String adminUserSessionID = generateAdminUserSessionID();
        return doLogin(account, password, adminUserSessionID);
    }

    @Process
    private LoginResult doLogin(String account, String password, String adminUserSessionID) {
        LoginResult loginResult = AdminUserService.login(this,
                account, password, new GMUserSession(adminUserSessionID),
                System.currentTimeMillis());
        return loginResult;
    }

    @Process
    private String generateAdminUserSessionID() {
        return adminUserSessionIDGeneratorRepository.take().generateId();
    }

    @Process
    public String getGmAccountBySessionId(String sessionId) {
        return AdminUserService.auth(this,
                sessionId);
    }

    @Process
    public void keepGmUserSessionAlive(String sessionId, long currentTime) {
        AdminUserService.keepSessionAlive(this,
                sessionId, currentTime);
    }

    @Process
    public boolean executeGmUserSessionCleanupTask(String taskName, long currentTime, int sessionBatchSize,
                                                   int maxSegmentExecutionTime, int maxTimeToTaskReady,
                                                   int sessionKeepAliveInterval) {
        List<String> allSessionId = gmUserSessionRepository.getAllSessionId();
        return AdminUserService.executeUserSessionCleanupTask(this,
                taskName, currentTime, sessionBatchSize, maxSegmentExecutionTime, maxTimeToTaskReady, sessionKeepAliveInterval, allSessionId);
    }


    public int getGMUserCount(String account, String name) {
        return gmUserRepository.getGMUserCount(account, name);
    }

    @Process
    public GMUser updateGMUser(String account, String password, String name) {
        GMUser gmUser = gmUserRepository.take(account);
        gmUser.setPassword(password);
        gmUser.setName(name);
        return gmUser;
    }

    @Process
    public GMUser deleteGMUserByAccount(String account) {
        return gmUserRepository.remove(account);
    }

    @Process
    public void logout(String account) {
        AdminUserService.logout(this, account);
    }

    @Override
    public AdminUserRepository getAdminUserRepository() {
        return gmUserRepository;
    }

    @Override
    public AdminUserSessionRepository getAdminUserSessionRepository() {
        return gmUserSessionRepository;
    }

    @Override
    public AdminUserCurrentSessionRepository getAdminUserCurrentSessionRepository() {
        return gmAdminUserCurrentSessionRepository;
    }

    @Override
    public AdminUserSessionAliveKeeperRepository getAdminUserSessionAliveKeeperRepository() {
        return gmAdminUserSessionAliveKeeperRepository;
    }

    @Override
    public ClearSessionTaskRepository getClearSessionTaskRepository() {
        return gmClearSessionTaskRepository;
    }

    @Override
    public ClearSessionTaskSegmentRepository getClearSessionTaskSegmentRepository() {
        return gmClearSessionTaskSegmentRepository;
    }

    @Override
    public ClearSessionTaskSegmentIDGeneratorRepository getClearSessionTaskSegmentIDGeneratorRepository() {
        return clearSessionTaskSegmentIDGeneratorRepository;
    }


}
