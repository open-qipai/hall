package com.hall.service.gm;

import com.hall.entity.gm.GMApi;
import com.hall.repository.GMApiRepository;
import com.hall.repository.impl.MongodbGMApiRepository;
import erp.annotation.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GMApiService {

    private GMApiRepository gmApiRepository;

    @Autowired
    public GMApiService(MongoTemplate mongoTemplate) {
        gmApiRepository = new MongodbGMApiRepository(mongoTemplate);
    }

    public List<GMApi> getGMApiList(String uri, int from, int size) {
        return gmApiRepository.queryAll(uri, from, size);
    }

    @Process
    public void addGMApi(String uri, String permissionId) {
        GMApi gmApi = new GMApi();
        gmApi.setUri(uri);
        gmApi.setPermissionId(permissionId);
        gmApiRepository.put(gmApi);
    }

    @Process
    public void updateGMApi(String uri, String permissionId) {
        GMApi gmApi = gmApiRepository.take(uri);
        gmApi.setPermissionId(permissionId);
    }

    @Process
    public void deleteGMApi(String uri) {
        gmApiRepository.remove(uri);
    }

    public int countGMApis(String uri) {
        return gmApiRepository.count(uri);
    }

    public GMApi getGMApi(String uri) {
        return gmApiRepository.find(uri);
    }
}
