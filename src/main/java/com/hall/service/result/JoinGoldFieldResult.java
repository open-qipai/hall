package com.hall.service.result;

public class JoinGoldFieldResult {
    private boolean success;
    private boolean balanceNotInRange;

    public boolean isBalanceNotInRange() {
        return balanceNotInRange;
    }

    public void setBalanceNotInRange(boolean balanceNotInRange) {
        this.balanceNotInRange = balanceNotInRange;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
