package com.hall.service.result;

import com.hall.entity.user.HallUser;
import dml.user.service.result.LoginByOpenIDResult;

public class LoginResult {
    private LoginByOpenIDResult loginByOpenIDResult;
    private HallUser hallUser;

    public LoginByOpenIDResult getLoginByOpenIDResult() {
        return loginByOpenIDResult;
    }

    public void setLoginByOpenIDResult(LoginByOpenIDResult loginByOpenIDResult) {
        this.loginByOpenIDResult = loginByOpenIDResult;
    }

    public HallUser getHallUser() {
        return hallUser;
    }

    public void setHallUser(HallUser hallUser) {
        this.hallUser = hallUser;
    }
}
