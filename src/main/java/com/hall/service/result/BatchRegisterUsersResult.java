package com.hall.service.result;

import com.hall.entity.user.HallUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BatchRegisterUsersResult {
    private Map<String, HallUser> openIdToHallUserMap = new HashMap<>();

    public void addNewUser(String openId, HallUser hallUser) {
        openIdToHallUserMap.put(openId, hallUser);
    }

    public Map<String, HallUser> getOpenIdToHallUserMap() {
        return openIdToHallUserMap;
    }

    public List<Long> getHallUserIdList() {
        List<Long> hallUserIdList = new ArrayList<>();
        for (HallUser hallUser : openIdToHallUserMap.values()) {
            hallUserIdList.add(hallUser.getId());
        }
        return hallUserIdList;
    }
}
