package com.hall.service;

import com.hall.entity.allocation.AllocationGameServer;
import com.hall.repository.AllocationGameServerRepository;
import dml.gameserverallocation.loadbased.entity.SelectedGameServer;
import dml.gameserverallocation.loadbased.repository.LoadBalancedAllocationGameServerRepository;
import dml.gameserverallocation.loadbased.repository.SelectedGameServerRepository;
import dml.gameserverallocation.loadbased.service.LoadBalancedAllocationService;
import dml.gameserverallocation.loadbased.service.repositoryset.LoadBalancedAllocationServiceRepositorySet;
import erp.annotation.Process;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllocationService implements
        LoadBalancedAllocationServiceRepositorySet {

    @Autowired
    private AllocationGameServerRepository allocationGameServerRepository;
    private SelectedGameServerRepository selectedGameServerRepository;

    @Autowired
    public AllocationService(RedisTemplate redisTemplate) {
        selectedGameServerRepository = RepositoryFactory.newInstance(SelectedGameServerRepository.class,
                new RedisRepository(redisTemplate, SelectedGameServer.class));

    }

    @Process
    public void selectBestGameServer(String gameId, long currTime, long maxSelectBestGameServerTime, long maxGameServerIdleTime) {
        boolean startSuccess = LoadBalancedAllocationService.startSelectBestGameServer(this,
                gameId, currTime, maxSelectBestGameServerTime);
        if (!startSuccess) {
            return;
        }
        List<AllocationGameServer> allGameServers = allocationGameServerRepository.getAllForGame(gameId);
        LoadBalancedAllocationService.selectBestGameServer(this,
                gameId, new ArrayList<>(allGameServers), currTime, maxGameServerIdleTime);
    }

    @Process
    public AllocationGameServer allocateGameServer(String gameId, int playerCount, long currentTime) {
        return (AllocationGameServer) LoadBalancedAllocationService.allocateGameServer(this,
                gameId, playerCount, currentTime);
    }

    @Process
    public void registerGameServer(String gameServerId, int maxPlayer, String gameId,
                                   String outerHost, String innerHost, long currentTime, long maxGameServerIdleTime) {
        AllocationGameServer gameServer = new AllocationGameServer();
        gameServer.setGameId(gameId);
        gameServer.setOuterHost(outerHost);
        gameServer.setInnerHost(innerHost);
        LoadBalancedAllocationService.addGameServer(this,
                gameServerId, maxPlayer, currentTime, maxGameServerIdleTime, gameServer);
    }

    @Process
    public void updateGameServerStatus(String gameServerId, int currentPlayers, long currentTime) {
        LoadBalancedAllocationService.updateGameServerCurrentPlayers(this,
                gameServerId, currentPlayers, currentTime, 5 * 1000L);
    }

    @Override
    public LoadBalancedAllocationGameServerRepository getLoadBalancedAllocationGameServerRepository() {
        return allocationGameServerRepository;
    }

    @Override
    public SelectedGameServerRepository getSelectedGameServerRepository() {
        return selectedGameServerRepository;
    }


}
