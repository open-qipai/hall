package com.hall.service;

import com.hall.entity.gameplay.GameMode;
import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.goldfield.GoldField;
import com.hall.repository.GoldFieldRepository;
import com.hall.repository.HallGamePlayIDGeneratorRepository;
import com.hall.repository.HallGamePlayRepository;
import dml.gamehallgameplay.entity.UserCurrentPlay;
import dml.gamehallgameplay.repository.GamePlayRepository;
import dml.gamehallgameplay.repository.UserCurrentPlayRepository;
import dml.gamehallgameplay.service.GamePlayService;
import dml.gamehallgameplay.service.repositoryset.GamePlayServiceRepositorySet;
import dml.id.entity.SnowflakeIdGenerator;
import erp.annotation.Process;
import erp.redis.RedisRepository;
import erp.repository.factory.RepositoryFactory;
import erp.repository.factory.SingletonRepositoryFactory;
import erp.repository.impl.mem.MemSingletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HallGamePlayService implements GamePlayServiceRepositorySet {

    @Autowired
    private HallGamePlayRepository hallGamePlayRepository;
    private UserCurrentPlayRepository userCurrentPlayRepository;
    private HallGamePlayIDGeneratorRepository hallGamePlayIDGeneratorRepository;
    @Autowired
    private GoldFieldRepository goldFieldRepository;

    @Autowired
    public HallGamePlayService(RedisTemplate redisTemplate) {
        userCurrentPlayRepository = RepositoryFactory.newInstance(UserCurrentPlayRepository.class,
                new RedisRepository(redisTemplate, UserCurrentPlay.class));
        hallGamePlayIDGeneratorRepository = SingletonRepositoryFactory.newInstance(HallGamePlayIDGeneratorRepository.class,
                new MemSingletonRepository(new SnowflakeIdGenerator(1L)));
    }

    @Process
    public HallGamePlay playJinbichangGame(List<Long> userIdList, String gameId, String gameServerGameId,
                                           String innerHost, String outerHost, String goldFieldId) {
        GoldField goldField = goldFieldRepository.find(goldFieldId);
        int difen = goldField.getDifen();
        GameMode gameMode = null;
        if (gameId.equals("wenzhou_mahjong")) {
            gameMode = GameMode.jinbichang_wzmj;
        }
        HallGamePlay hallGamePlay = new HallGamePlay();
        hallGamePlay.setId(hallGamePlayIDGeneratorRepository.take().generateId());
        hallGamePlay.playGame(gameMode, gameId, gameServerGameId, innerHost, outerHost, userIdList, difen);
        GamePlayService.playGame(this, hallGamePlay);
        return hallGamePlay;
    }

    public HallGamePlay getCurrentGamePlayForUser(long userId) {
        return (HallGamePlay) GamePlayService.findUserCurrentGamePlay(this, userId);
    }

    public HallGamePlay getGamePlay(long gamePlayId) {
        return hallGamePlayRepository.find(gamePlayId);
    }

    @Process
    public HallGamePlay endGame(long gamePlayId) {
        GamePlayService.endGame(this, gamePlayId);
        return hallGamePlayRepository.find(gamePlayId);
    }

    @Override
    public GamePlayRepository getGamePlayRepository() {
        return hallGamePlayRepository;
    }

    @Override
    public UserCurrentPlayRepository getUserCurrentPlayRepository() {
        return userCurrentPlayRepository;
    }


}
