package com.hall.service;

import com.hall.entity.gameplay.HallGamePlay;
import com.hall.entity.money.MoneyAccount;
import com.hall.entity.money.MoneyBillItem;
import com.hall.repository.GameCurrencyAccountBillItemIdGeneratorRepository;
import com.hall.repository.HallGamePlayRepository;
import com.hall.repository.MoneyAccountRepository;
import com.hall.repository.MoneyBillItemRepository;
import dml.gamecurrency.repository.GameCurrencyAccountBillItemRepository;
import dml.gamecurrency.repository.GameCurrencyAccountIdGeneratorRepository;
import dml.gamecurrency.repository.GameCurrencyAccountRepository;
import dml.gamecurrency.repository.GameUserCurrencyAccountsRepository;
import dml.gamecurrency.service.GameCurrencyAccountingService;
import dml.gamecurrency.service.repositoryset.GameCurrencyAccountingServiceRepositorySet;
import erp.annotation.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SettleService implements
        GameCurrencyAccountingServiceRepositorySet {

    @Autowired
    private MoneyAccountRepository moneyAccountRepository;
    @Autowired
    private GameCurrencyAccountIdGeneratorRepository gameCurrencyAccountIdGeneratorRepository;
    @Autowired
    private GameUserCurrencyAccountsRepository gameUserCurrencyAccountsRepository;
    @Autowired
    private MoneyBillItemRepository moneyBillItemRepository;
    @Autowired
    private GameCurrencyAccountBillItemIdGeneratorRepository gameCurrencyAccountBillItemIdGeneratorRepository;

    @Autowired
    private HallGamePlayRepository hallGamePlayRepository;


    public void settleJinbichangWzmj(long gamePlayId, long huPlayerId) {
        doSettleJinbichangWzmj(gamePlayId, huPlayerId, generateBillItemId());
    }

    @Process
    private void doSettleJinbichangWzmj(long gamePlayId, long huPlayerId, long newBillItemId) {
        HallGamePlay hallGamePlay = hallGamePlayRepository.find(gamePlayId);
        int difen = hallGamePlay.getDifen();
        int playerCount = hallGamePlay.getPlayerIdList().size();
        int winGold = difen * playerCount;
        MoneyBillItem moneyBillItem = new MoneyBillItem(newBillItemId);
        moneyBillItem.setReason("win_gold_field_wzmj");
        GameCurrencyAccountingService.deposit(this,
                huPlayerId, "gold", winGold + "", new MoneyAccount(), moneyBillItem);
    }

    @Process
    private long generateBillItemId() {
        return gameCurrencyAccountBillItemIdGeneratorRepository.take().generateId();
    }

    @Override
    public GameCurrencyAccountRepository getGameCurrencyAccountRepository() {
        return moneyAccountRepository;
    }

    @Override
    public GameCurrencyAccountIdGeneratorRepository getGameCurrencyAccountIdGeneratorRepository() {
        return gameCurrencyAccountIdGeneratorRepository;
    }

    @Override
    public GameUserCurrencyAccountsRepository getGameUserCurrencyAccountsRepository() {
        return gameUserCurrencyAccountsRepository;
    }

    @Override
    public GameCurrencyAccountBillItemRepository getGameCurrencyAccountBillItemRepository() {
        return moneyBillItemRepository;
    }

}
