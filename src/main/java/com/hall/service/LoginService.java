package com.hall.service;

import com.hall.entity.user.HallUser;
import com.hall.entity.user.HallUserSession;
import com.hall.repository.*;
import com.hall.service.result.BatchRegisterUsersResult;
import com.hall.service.result.LoginResult;
import dml.id.entity.IntStringIdGenerator;
import dml.user.entity.OpenIDUserBind;
import dml.user.entity.UserSession;
import dml.user.repository.*;
import dml.user.service.AuthService;
import dml.user.service.LoginByOpenIDService;
import dml.user.service.repositoryset.AuthServiceRepositorySet;
import dml.user.service.repositoryset.LoginByOpenIDServiceRepositorySet;
import dml.user.service.result.LoginByOpenIDResult;
import erp.annotation.Process;
import erp.redis.RedisSingletonRepository;
import erp.redis.pipeline.RedisPipeline;
import erp.repository.factory.SingletonRepositoryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LoginService implements LoginByOpenIDServiceRepositorySet, AuthServiceRepositorySet {

    @Autowired
    private HallUserRepository hallUserRepository;
    @Autowired
    private HallOpenIDUserBindRepository hallOpenIDUserBindRepository;
    @Autowired
    private UserIDGeneratorRepository userIDGeneratorRepository;
    @Autowired
    private HallUserSessionRepository hallUserSessionRepository;
    @Autowired
    private UserSessionIDGeneratorRepository userSessionIDGeneratorRepository;
    @Autowired
    private UserCurrentSessionRepository hallUserCurrentSessionRepository;
    @Autowired
    private UserSessionAliveKeeperRepository hallUserSessionAliveKeeperRepository;
    private UserSerialNumberGeneratorRepository userSerialNumberGeneratorRepository;


    @Autowired
    public LoginService(MongoTemplate mongoTemplate, RedisTemplate redisTemplate) {
        userSerialNumberGeneratorRepository = SingletonRepositoryFactory.newInstance(UserSerialNumberGeneratorRepository.class,
                new RedisSingletonRepository(redisTemplate, new IntStringIdGenerator(10), "userSerialNumberGeneratorRepository"));
    }


    public LoginResult login(String openID, long currentTime, String nickname) {
        return doLogin(openID, currentTime, nickname, generateNewSessionId());
    }

    @Process
    @RedisPipeline
    private LoginResult doLogin(String openID, long currentTime, String nickname, String newSessionId) {
        LoginResult result = new LoginResult();
        LoginByOpenIDResult loginByOpenIDResult = LoginByOpenIDService.loginByOpenID(this,
                openID,
                currentTime,
                new HallUser(),
                new HallUserSession(newSessionId));
        result.setLoginByOpenIDResult(loginByOpenIDResult);
        UserSession newUserSession = loginByOpenIDResult.getNewUserSession();
        if (!loginByOpenIDResult.isCreateNewUser()) {
            result.setHallUser(hallUserRepository.find((Long) newUserSession.getUserID()));
            return result;
        }
        HallUser user = hallUserRepository.take((Long) newUserSession.getUserID());
        user.setNickname(nickname);
        String userSerialNumber = userSerialNumberGeneratorRepository.take().generateId();
        user.setSerialNumber(userSerialNumber);
        result.setHallUser(user);
        return result;
    }

    @Process
    private String generateNewSessionId() {
        return userSessionIDGeneratorRepository.take().generateId();
    }

    public Long getUserIdBySessionId(String sessionId) {
        return (Long) AuthService.auth(this, sessionId);
    }

    @Process
    public void keepHallUserSessionAlive(String sessionId, long currentTime) {
        AuthService.keepSessionAlive(this, sessionId, currentTime);
    }

    @Process
    public BatchRegisterUsersResult batchRegisterUsers(List<Map> inputUserList) {
        BatchRegisterUsersResult result = new BatchRegisterUsersResult();
        for (Map inputUser : inputUserList) {
            String openId = (String) inputUser.get("openId");
            String nickname = (String) inputUser.get("nickname");
            HallUser hallUser = new HallUser();
            hallUser.setNickname(nickname);
            hallUser = (HallUser) LoginByOpenIDService.registerByOpenID(this,
                    openId, hallUser);
            String userSerialNumber = userSerialNumberGeneratorRepository.take().generateId();
            hallUser.setSerialNumber(userSerialNumber);
            result.addNewUser(openId, hallUser);
        }
        return result;
    }

    @Process
    public void deregisterUser(long userId) {
        List<OpenIDUserBind> openIDUserBinds = hallOpenIDUserBindRepository.findByUserId(userId);
        for (OpenIDUserBind openIDUserBind : openIDUserBinds) {
            hallOpenIDUserBindRepository.remove(openIDUserBind.getOpenID());
        }
        hallUserRepository.remove(userId);
    }

    public int countHallUserSession() {
        return hallUserSessionRepository.count();
    }

    public HallUser getUserById(long userId) {
        return hallUserRepository.find(userId);
    }

    public List<HallUser> findHallUserList(List<Long> userIdList) {
        List<HallUser> hallUserList = hallUserRepository.findList(userIdList);
        // 保持顺序
        Map<Long, HallUser> hallUserMap = hallUserList.stream().collect(
                java.util.stream.Collectors.toMap(HallUser::getId, (p) -> p));
        return userIdList.stream().map((userId) -> hallUserMap.get(userId)).collect(java.util.stream.Collectors.toList());
    }

    @Override
    public OpenIDUserBindRepository getOpenIDUserBindRepository() {
        return hallOpenIDUserBindRepository;
    }

    @Override
    public UserIDGeneratorRepository getUserIDGeneratorRepository() {
        return userIDGeneratorRepository;
    }

    @Override
    public UserRepository getUserRepository() {
        return hallUserRepository;
    }

    @Override
    public UserSessionRepository getUserSessionRepository() {
        return hallUserSessionRepository;
    }

    @Override
    public UserCurrentSessionRepository getUserCurrentSessionRepository() {
        return hallUserCurrentSessionRepository;
    }

    @Override
    public UserSessionAliveKeeperRepository getUserSessionAliveKeeperRepository() {
        return hallUserSessionAliveKeeperRepository;
    }


}
